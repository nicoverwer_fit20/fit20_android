package fit20.app.testHelpers.screens

import com.kaspersky.kaspresso.screens.KScreen
import fit20.app.R
import fit20.app.presentation.standby.StandbyFragment

/**
 * Kakao wrapper around @see [StandbyFragment]
 *
 * @author a.kokuliuk
 * @since v0.0
 */
object StandByScreen : KScreen<StandByScreen>() {
    override val layoutId = R.layout.fragment_standby
    override val viewClass = StandbyFragment::class.java
}
