package fit20.app.presentation.base

import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * Base navigation screen class
 *
 * @author a.kokuliuk
 * @since v0.0
 */
abstract class BaseAppScreen : SupportAppScreen()
