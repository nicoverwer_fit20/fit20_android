package fit20.app.presentation.base

import androidx.appcompat.app.AppCompatActivity
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

/**
 * Base activity class
 *
 * @author a.kokuliuk
 * @since v0.0
 */
abstract class BaseActivity : AppCompatActivity(), CoroutineScope {
    private val disposable = CompositeDisposable()
    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job

    fun Disposable.bind(){
        disposable.add(this)
    }

    override fun onStop() {
        job.complete()
        disposable.clear()
        super.onStop()
    }
}
