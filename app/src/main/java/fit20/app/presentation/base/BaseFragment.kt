package fit20.app.presentation.base

import androidx.fragment.app.Fragment
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.core.KoinComponent
import kotlin.coroutines.CoroutineContext

/**
 * Base fragment class
 *
 * @author a.kokuliuk
 * @since v0.0
 */
abstract class BaseFragment : Fragment(), KoinComponent, CoroutineScope {
    private val disposable = CompositeDisposable()
    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    fun Disposable.bind() {
        disposable.add(this)
    }

    override fun onStop() {
        job.cancel()
        disposable.clear()
        super.onStop()
    }
}
