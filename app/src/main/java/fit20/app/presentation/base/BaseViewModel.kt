package fit20.app.presentation.base

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

/**
 * Base MVI view model
 *
 * @property T state class
 *
 * @author a.kokuliuk
 * @since v0.0
 */
abstract class BaseViewModel<T> : ViewModel(), LifecycleObserver, CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext =
        Dispatchers.Default + job

    private val disposable = CompositeDisposable()
    val state = BehaviorSubject.create<T>()

    override fun onCleared() {
        job.complete()
        disposable.clear()
        super.onCleared()
    }

    /**
     * Mutate VM state
     *
     * @param block lambda that return state @see [T]
     *
     * @since v0.0
     */
    inline fun newState(block: (T) -> T) {
        state.onNext(block(state.value))
    }

    /**
     * Bind disposable that must be cleared before vm destroy
     *
     * @since v0.0
     */
    fun Disposable.bind() = disposable.add(this)
}
