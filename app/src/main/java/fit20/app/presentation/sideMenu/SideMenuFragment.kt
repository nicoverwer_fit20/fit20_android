package fit20.app.presentation.sideMenu

import android.Manifest
import android.graphics.drawable.LevelListDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.markodevcic.peko.Peko
import com.markodevcic.peko.PermissionResult
import com.markodevcic.peko.requestPermissionsAsync
import fit20.app.R
import fit20.app.databinding.FragmentServiceBinding
import fit20.app.databinding.FragmentServiceBinding.inflate
import fit20.app.presentation.base.BaseFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_service.*
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class SideMenuFragment : BaseFragment() {
    private val viewModel: SideMenuViewModel by viewModel()
    private lateinit var binding: FragmentServiceBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = inflate(inflater, container, false)
        binding.sensorRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.usb -> {
                    viewModel.switchToUsbSensor()
                }
                R.id.ble -> {
                    launch {
                        if (!Peko.isRequestInProgress()) {
                            if (requestPermissionsAsync(
                                    Manifest.permission.ACCESS_FINE_LOCATION
                                ) is PermissionResult.Granted
                            ) {
                                viewModel.switchToBleSensor()
                            } else {
                                binding.sensorRadioGroup.clearCheck()
                            }
                        }
                    }
                }
            }
        }
        return binding.root
    }


    override fun onResume() {
        super.onResume()

        val indicatorDrawable = ivSensorStatusIndicator.drawable as LevelListDrawable

        viewModel.state.observeOn(AndroidSchedulers.mainThread())
            .subscribe { state ->
                binding.apply {
                    ipAddr = state.ipAddress
                    appVersion = state.version
                    connectionState = getString(state.sensorStateTitleRes)
                    connectionMessage = state.sensorStateMessage ?: ""

                    //Use direct access to vm to prevent race conditions
                    when (viewModel.getDataSource()) {
                        SideMenuState.CurrentSensor.USB -> {
                            if (sensorRadioGroup.checkedRadioButtonId != R.id.usb)
                                sensorRadioGroup.check(R.id.usb)
                        }

                        SideMenuState.CurrentSensor.BLE -> {
                            if (sensorRadioGroup.checkedRadioButtonId != R.id.ble)
                                sensorRadioGroup.check(R.id.ble)
                        }
                    }
                    when (state.sensorStateTitleRes) {
                        R.string.sensor_connected -> indicatorDrawable.level = 0
                        R.string.sensor_disconnected -> indicatorDrawable.level = 1
                        R.string.sensor_failed -> indicatorDrawable.level = 2
                    }

                }
            }.bind()
    }
}
