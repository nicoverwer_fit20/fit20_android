package fit20.app.presentation.sideMenu

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.text.format.Formatter
import fit20.app.BuildConfig
import fit20.app.R
import fit20.app.data.EncoderDataSource
import fit20.app.data.wrapper.WrapperEncoderDataSource
import fit20.app.presentation.base.BaseViewModel
import kotlinx.coroutines.launch

/**
 * Side menu view model
 *
 * Shows IP address and usb sensor connection state
 *
 * @author a.kokuliuk
 * @since
 */
class SideMenuViewModel(
    private val context: Context,
    private val encoderDataSource: WrapperEncoderDataSource
) : BaseViewModel<SideMenuState>() {

    private val wifiManager = context.applicationContext.getSystemService(Context.WIFI_SERVICE)
            as WifiManager
    private val networkReceiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            updateIpAddress()
        }
    }

    init {
        updateIpAddress()
        encoderDataSource.getConnectionStateObservable()
            .subscribe { externalState ->
                newState { prevState ->
                    when (externalState) {
                        is EncoderDataSource.EncoderConnectionState.Connected ->
                            prevState.copy(
                                sensorStateTitleRes = R.string.sensor_connected,
                                sensorStateMessage = null
                            )
                        is EncoderDataSource.EncoderConnectionState.Disconnected ->
                            prevState.copy(
                                sensorStateTitleRes = R.string.sensor_disconnected,
                                sensorStateMessage = null
                            )
                        is EncoderDataSource.EncoderConnectionState.Error ->
                            prevState.copy(
                                sensorStateTitleRes = R.string.sensor_failed,
                                sensorStateMessage = externalState.toString()
                            )
                        is EncoderDataSource.EncoderConnectionState.ConnectedInfo ->
                            prevState.copy(
                                sensorStateTitleRes = R.string.sensor_connected,
                                sensorStateMessage = externalState.toString()
                            )
                        is EncoderDataSource.EncoderConnectionState.DisconnectedInfo ->
                            prevState.copy(
                                sensorStateTitleRes = R.string.sensor_disconnected,
                                sensorStateMessage = externalState.toString()
                            )
                    }
                }
            }.bind()
        encoderDataSource
            .currentDataSourceType
            .subscribe {
                newState { it }
            }
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(networkReceiver, filter)
    }

    fun switchToBleSensor() {
        launch {
            encoderDataSource.switchDataSource(WrapperEncoderDataSource.EncoderDataSourceType.BLUETOOTH)
        }
    }

    fun updateIpAddress(){
        newState { prevState : SideMenuState? ->
            SideMenuState(
                Formatter.formatIpAddress(wifiManager.connectionInfo.ipAddress),
                prevState?.sensorStateTitleRes ?: R.string.sensor_unknown,
                prevState?.sensorStateMessage ?: "",
                BuildConfig.VERSION_NAME
            )
        }
    }

    override fun onCleared() {
        try {
            context.unregisterReceiver(networkReceiver)
        }catch (t: Throwable){
            //Prevent app crash
            t.printStackTrace()
        }
        super.onCleared()
    }

    fun switchToUsbSensor() {
        launch {
            encoderDataSource.switchDataSource(WrapperEncoderDataSource.EncoderDataSourceType.USB)
        }
    }

    fun getDataSource(): SideMenuState.CurrentSensor =
        if (encoderDataSource.currentDataSourceType.value == WrapperEncoderDataSource.EncoderDataSourceType.BLUETOOTH)
            SideMenuState.CurrentSensor.BLE
        else
            SideMenuState.CurrentSensor.USB
}
