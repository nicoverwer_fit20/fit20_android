package fit20.app.presentation.sideMenu

data class SideMenuState(
    val ipAddress: String,
    val sensorStateTitleRes: Int,
    val sensorStateMessage: String?,
    val version: String
) {
    enum class CurrentSensor {
        BLE, USB
    }
}