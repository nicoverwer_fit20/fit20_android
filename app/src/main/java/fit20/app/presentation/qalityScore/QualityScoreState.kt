package fit20.app.presentation.qalityScore

import fit20.app.exercise.QualityScore

data class QualityScoreState(
    val isLoading: Boolean = true,
    val qualityScore: QualityScore? = null
)
