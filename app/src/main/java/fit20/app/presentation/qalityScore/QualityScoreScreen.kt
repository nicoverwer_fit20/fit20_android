package fit20.app.presentation.qalityScore

import fit20.app.exercise.QualityScore
import fit20.app.presentation.base.BaseAppScreen

class QualityScoreScreen() : BaseAppScreen() {
    override fun getFragment() = QualityScoreFragment()
}
