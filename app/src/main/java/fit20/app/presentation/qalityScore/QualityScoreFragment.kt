package fit20.app.presentation.qalityScore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fit20.app.R
import fit20.app.databinding.FragmentQsBinding
import fit20.app.presentation.base.BaseFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import org.koin.android.viewmodel.ext.android.viewModel

class QualityScoreFragment : BaseFragment() {
    private val viewModel: QualityScoreViewModel by viewModel()
    private lateinit var binding: FragmentQsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentQsBinding.inflate(inflater, container, false).apply {
            isLoading = false
            qualityScore = 0
            rhythm = 0
            tempo = 0
            range = 0
        }
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.state
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                binding.apply {
                    isLoading = it.isLoading
                    qualityScore = it.qualityScore?.total ?: 0
                    rhythm = it.qualityScore?.rhythm ?: 0
                    tempo = it.qualityScore?.tempo ?: 0
                    range = it.qualityScore?.range ?: 0
                }
            }.bind()
    }
}
