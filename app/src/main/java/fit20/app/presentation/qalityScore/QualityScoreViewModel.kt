package fit20.app.presentation.qalityScore

import androidx.lifecycle.LifecycleObserver
import fit20.app.appState.AppStateInteractor
import fit20.app.presentation.base.BaseViewModel

class QualityScoreViewModel(
    private val appStateInteractor: AppStateInteractor
) : BaseViewModel<QualityScoreState>() {

    init {
        newState {
            QualityScoreState()
        }

        appStateInteractor.getAppState()
            .subscribe { appState ->
                if (appState is AppStateInteractor.AppState.QualityScoreState) {
                    newState {
                        it.copy(
                            isLoading = appState.qualityScore == null,
                            qualityScore = appState.qualityScore
                        )
                    }
                }
            }.bind()
    }
}
