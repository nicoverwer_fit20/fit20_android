package fit20.app.presentation.activity

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import fit20.app.R
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

class Navigator : SupportAppNavigator {
    constructor(activity: FragmentActivity, containerId: Int) : super(activity, containerId)
    constructor(
        activity: FragmentActivity,
        fragmentManager: FragmentManager,
        containerId: Int
    ) : super(activity, fragmentManager, containerId)

    override fun setupFragmentTransaction(
        command: Command,
        currentFragment: Fragment?,
        nextFragment: Fragment?,
        fragmentTransaction: FragmentTransaction
    ) {
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
    }
}