package fit20.app.presentation.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import fit20.app.R
import fit20.app.appState.AppStateInteractor
import fit20.app.presentation.base.BaseActivity
import fit20.app.presentation.exercise.ExerciseScreen
import fit20.app.presentation.qalityScore.QualityScoreScreen
import fit20.app.presentation.standby.StandbyAppScreen
import fit20.app.utils.IntentListener
import fit20.app.utils.LifecycleObserversHolder
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import java.util.concurrent.TimeUnit

/**
 * Single activity in the application
 *
 * @author a.kokuliuk
 * @since v0.0
 */
class MainActivity : BaseActivity() {

    private val intentListeners: IntentListener.IntentListenersHolder by inject()
    private val lifecycleObservers: LifecycleObserversHolder by inject()
    private val navigatorHolder: NavigatorHolder by inject()
    private val appStateInteractor: AppStateInteractor by inject()
    private val router: Router by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        lifecycleObservers.observers.forEach { lifecycle.addObserver(it) }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!canWriteSettings(this)) {
            requestSettingsPermission(this)
        }
    }

    private fun setFullScreenMode() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    private fun subscribeToAppStates() {
        appStateInteractor
            .getAppState()
            .throttleLast(160, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                when (it) {
                    is AppStateInteractor.AppState.StandbyState ->
                        router.replaceScreen(StandbyAppScreen())
                    is AppStateInteractor.AppState.ExerciseState ->
                        router.replaceScreen(ExerciseScreen())
                    is AppStateInteractor.AppState.QualityScoreState ->
                        router.replaceScreen(QualityScoreScreen())
                }
            }.bind()

        appStateInteractor
            .getBrightness()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setBrightness(this, it)
            }.bind()
    }

    override fun onDestroy() {
        setBrightness(this, resources.getInteger(R.integer.high_brightness))
        lifecycleObservers.observers.forEach { lifecycle.removeObserver(it) }
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(Navigator(this, R.id.fragment_container))
        subscribeToAppStates()
        setFullScreenMode()
        setBrightness(this, resources.getInteger(R.integer.low_brightness))
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
        setBrightness(this, resources.getInteger(R.integer.high_brightness))
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        launch {
            intentListeners.listeners.forEach {
                it.onNewIntent(this@MainActivity, intent)
            }
        }
    }

    private fun setBrightness(context: Context, light: Int) {
        try {
            val brightnessMode = Settings.System.getInt(
                context.contentResolver,
                Settings.System.SCREEN_BRIGHTNESS_MODE
            )
            if (brightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
                Settings.System.putInt(
                    context.contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
                )
            }
            Settings.System.putInt(
                context.contentResolver,
                Settings.System.SCREEN_BRIGHTNESS,
                light
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun canWriteSettings(context: Context): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.System.canWrite(context)
    }

    private fun requestSettingsPermission(activity: Activity) {
        if (canWriteSettings(context = activity))
            return
        try {
            val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
            intent.data = Uri.parse("package:" + activity.packageName)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            activity.startActivityForResult(intent, 0)
        } catch (e: Exception) {
            Log.e("requestCanWriteSettings", "requestCanWriteSettings $e")
        }
    }
}
