package fit20.app.presentation.standby

import android.graphics.drawable.LevelListDrawable
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fit20.app.R
import fit20.app.databinding.FragmentStandbyBinding
import fit20.app.presentation.base.BaseFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_standby.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * StandBy fragment
 *
 * @author a.kokuliuk
 * @since v0.0
 */
class StandbyFragment : BaseFragment(){

    lateinit var binding: FragmentStandbyBinding
    private val viewModel: StandbyViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       binding = FragmentStandbyBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()

        val indicatorDrawable = ivStandbySensorStatusIndicator.drawable as LevelListDrawable

        viewModel.state.observeOn(AndroidSchedulers.mainThread())
            .subscribe { state ->
                binding.apply {

                    when (state.sensorStateTitleRes) {
                        R.string.sensor_connected -> indicatorDrawable.level = 0
                        R.string.sensor_disconnected -> indicatorDrawable.level = 1
                        R.string.sensor_failed -> indicatorDrawable.level = 2
                    }

                    // https://stackoverflow.com/questions/2734270/how-to-make-links-in-a-textview-clickable
                    ivAppUpdatemessage.text = Html.fromHtml(state.appUpdateMessage)
                    ivAppUpdatemessage.movementMethod = LinkMovementMethod.getInstance()
                    //Linkify.addLinks(ivAppUpdatemessage, Linkify.ALL)

                }
            }.bind()
    }
}
