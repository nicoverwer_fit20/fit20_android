package fit20.app.presentation.standby

import fit20.app.presentation.base.BaseAppScreen

/**
 * StandBy screen class
 *
 * @author a.kokuliuk
 * @since v0.0
 */
class StandbyAppScreen(
) : BaseAppScreen() {
    override fun getFragment() = StandbyFragment()
}
