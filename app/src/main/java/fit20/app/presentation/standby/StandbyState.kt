package fit20.app.presentation.standby

import fit20.app.presentation.base.BaseAppScreen
import fit20.app.utils.StateEvent

/**
 * StandBy fragment state
 *
 * @property replaceScreen navigation command (navigate to new screen and drop current from backstack)
 *
 * @author a.kokuliuk
 * @since v0.0
 */
data class StandbyState(
    val replaceScreen: StateEvent<BaseAppScreen>? = null,
    val sensorStateTitleRes: Int,
    val appUpdateMessage: String
)
