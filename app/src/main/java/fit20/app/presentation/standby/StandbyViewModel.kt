package fit20.app.presentation.standby

import android.content.Context
import android.graphics.drawable.LevelListDrawable
import fit20.app.R
import fit20.app.appState.AppStateInteractor
import fit20.app.data.EncoderDataSource
import fit20.app.data.wrapper.WrapperEncoderDataSource
import fit20.app.presentation.base.BaseViewModel
import fit20.app.utils.AppUpdater
import kotlinx.android.synthetic.main.fragment_standby.*

/**
 * StandBy view model
 */
class StandbyViewModel(
    private val context: Context,
    private val encoderDataSource: WrapperEncoderDataSource,
    private val updater: AppUpdater
) : BaseViewModel<StandbyState>() {

    init {
        encoderDataSource.getConnectionStateObservable()
            .subscribe { externalState ->
                setSensorConnectionState(externalState)
            }.bind()

        // Check for app update once.
        updater.check().subscribe { update, error ->
            if (update != null) {
                setUpdateMessage("<a href=\"${update.url}\">${context.resources.getString(R.string.update_available)}</a>")
            }
        }
    }

    fun setSensorConnectionState(sensorState: EncoderDataSource.EncoderConnectionState) {
        newState { prevState : StandbyState? ->
            StandbyState(
                prevState?.replaceScreen,
                when (sensorState) {
                    is EncoderDataSource.EncoderConnectionState.Connected -> R.string.sensor_connected
                    is EncoderDataSource.EncoderConnectionState.Disconnected -> R.string.sensor_disconnected
                    is EncoderDataSource.EncoderConnectionState.Error -> R.string.sensor_failed
                    is EncoderDataSource.EncoderConnectionState.ConnectedInfo -> R.string.sensor_connected
                    is EncoderDataSource.EncoderConnectionState.DisconnectedInfo -> R.string.sensor_disconnected
                },
                prevState?.appUpdateMessage ?: ""
            )
        }
    }

    fun setUpdateMessage(message: String) {
        newState { prevState : StandbyState? ->
            StandbyState(
                prevState?.replaceScreen,
                prevState?.sensorStateTitleRes ?: R.string.sensor_unknown,
                message
            )
        }
    }

}
