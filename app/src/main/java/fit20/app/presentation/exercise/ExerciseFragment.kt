package fit20.app.presentation.exercise

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fit20.app.R
import fit20.app.appState.AppStateInteractor
import fit20.app.databinding.FragmentExerciseBinding
import fit20.app.exercise.Exercise
import fit20.app.presentation.base.BaseFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import org.koin.core.inject
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue

class ExerciseFragment : BaseFragment() {
    private val exercise: Exercise by inject()
    private val appState: AppStateInteractor by inject()

    private lateinit var binding: FragmentExerciseBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //TODO:
        binding = FragmentExerciseBinding.inflate(inflater, container, false)

        binding.pbReference.apply {
            progress = {
                exercise.getExerciseData().referencePosition.toFloat()
            }
        }

        binding.pbYou.apply {
            progress = {
                exercise.getExerciseData().actualPosition.toFloat()
            }

            tracking = {
                exercise.getExerciseData().tracking.toFloat().absoluteValue
            }
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        Flowable.interval(120, TimeUnit.MILLISECONDS)
            .onBackpressureLatest()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                exercise.getExerciseData().let {
                    when {
                        it.count == null -> {
                            binding.isCountdownVisible = false
                            binding.isTimerVisible = false
                        }

                        it.halfRepetition == -1 -> {
                            binding.isCountdownVisible = false
                            binding.isTimerVisible = true
                            binding.timer = it.count.toString()
                        }

                        it.count < 1 -> {
                            binding.isCountdownVisible = true
                            binding.isTimerVisible = false
                            binding.countdown =
                                if (it.count == 0) getString(R.string.go) else ((it.count * -1)).toString()
                        }

                        else -> {
                            binding.isCountdownVisible = true
                            binding.isTimerVisible = false
                            binding.countdown = it.count.toString()
                        }
                    }
                }
            }.bind()

        appState.getMachineIdentity()
            .onBackpressureLatest()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                binding.tvExName.text = it.machineName
            }.bind()

        appState.getMemberIdentity()
            .onBackpressureLatest()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                binding.tvUserName.text = it.name
            }.bind()

        appState.getColorScheme()
            .onBackpressureLatest()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                binding.pbYou.setColorMap(it)
            }.bind()
    }

    override fun onPause() {
        binding.pbYou.close()
        binding.pbReference.close()
        super.onPause()
    }
}
