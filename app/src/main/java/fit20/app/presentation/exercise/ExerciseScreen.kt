package fit20.app.presentation.exercise

import androidx.fragment.app.Fragment
import fit20.app.presentation.base.BaseAppScreen

class ExerciseScreen: BaseAppScreen(){
    override fun getFragment() = ExerciseFragment()
}
