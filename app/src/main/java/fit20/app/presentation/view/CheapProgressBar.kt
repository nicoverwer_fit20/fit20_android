package fit20.app.presentation.view

import android.animation.ArgbEvaluator
import android.content.Context
import android.graphics.*
import android.graphics.drawable.LayerDrawable
import android.util.AttributeSet
import android.view.TextureView
import android.view.View
import fit20.app.R
import java.util.*
import kotlin.math.abs

/**
 * SurfaceView based progress bar
 *
 * @property progress lambda which will provide progress value, must lock mothing
 * @property drawInterval time between frame rendering
 *
 * @author a.kokuliuk, b.savrasov
 * @since v0.0
 */
class CheapProgressBar(ctx: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    TextureView(ctx, attrs, defStyleAttr), TextureView.SurfaceTextureListener {

    private val argbEvaluator = ArgbEvaluator()
    private var colorMap = TreeMap<Float, Int>()
    private val graphics: LayerDrawable
    private val linesWidth: Float
    private var drawThread: DrawThread? = null
    private var lastProgress: Float = Float.NaN
    private var lastTracking: Float = Float.NaN

    var progress: (() -> Float)? = { 0f }
    var tracking: (() -> Float)? = { 0f }
    var drawInterval: Long = 1000 / 60
    var overflow: Float = .1f
    var underflow: Float = .1f

    constructor(ctx: Context, attrs: AttributeSet?) : this(ctx, attrs, 0)
    constructor(ctx: Context) : this(ctx, null, 0)

    init {
        surfaceTextureListener = this

        context.theme.obtainStyledAttributes(attrs, R.styleable.CustomProgressBar, 0, 0).apply {
            try {
                graphics =
                    getDrawable(R.styleable.CustomProgressBar_progressDrawable) as LayerDrawable
                linesWidth =
                    getDimension(R.styleable.CustomProgressBar_underflowOverflowLinesWidth, 1f)
            } finally {
                recycle()
            }
        }
    }

    private val whitePaint = Paint().apply {
        color = Color.WHITE
        style = Paint.Style.FILL
    }

    private val blackLine = Paint().apply {
        color = Color.BLACK
        style = Paint.Style.STROKE
        strokeWidth = linesWidth
    }

    private val whiteLine = Paint(blackLine).apply {
        color = Color.WHITE
    }

    fun setColorMap(cm: Map<Float, Int>) {
        colorMap.clear()
        colorMap.putAll(cm)
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) {
        updateLinesStyle(height)
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {
        //do nothing
    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
        return if (drawThread == null) {
            true
        } else {
            drawThread?.textureRelease = surface
            false
        }
    }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
        updateLinesStyle(height)
        visibility = View.INVISIBLE
        drawThread = DrawThread()
        drawThread!!.start()
    }

    private fun updateLinesStyle(height: Int) {
        val quaterHeight = height / 4f
        val dashEffect = DashPathEffect(
            floatArrayOf(
                quaterHeight * 0.75f,
                quaterHeight * 0.25f
            ), -quaterHeight * 0.125f
        )
        blackLine.pathEffect = dashEffect
        whiteLine.pathEffect = dashEffect
    }

    private fun calculateColor(): Int? {
        val key = abs(lastTracking)
        val lEntry = colorMap.lowerEntry(key)
        val hEntry = colorMap.higherEntry(key)

        colorMap[key]?.also {
            return it
        }

        return if (lEntry != null && hEntry != null) {
            val fraction = (key - lEntry.key) / (hEntry.key - lEntry.key)
            argbEvaluator.evaluate(fraction, lEntry.value, hEntry.value) as Int
        } else if (lEntry != null) {
            lEntry.value
        } else if (hEntry != null) {
            hEntry.value
        } else {
            null
        }
    }

    private fun actualDraw(canvas: Canvas) {
        val totalUnits = underflow + 1f + overflow
        val unitInPixels = canvas.width / totalUnits
        val barStartInPixels = 0f
        val barEndInPixels = unitInPixels * (underflow + lastProgress).coerceIn(0f, totalUnits)
        val calibrationStartInPixels = unitInPixels * underflow
        val calibrationEndInPixels = unitInPixels * (1f + overflow)
        val color = calculateColor()

        canvas.apply {
            val background = graphics.getDrawable(0)
            val foreground = graphics.getDrawable(1)

            canvas.drawRect(
                0f,
                0f,
                canvas.width.toFloat(),
                canvas.height.toFloat(),
                whitePaint
            )

            background.bounds = Rect(0, 0, canvas.width, canvas.height)
            background.draw(this)

            val barRect = if (barEndInPixels > barStartInPixels) {
                Rect(barStartInPixels.toInt(), 0, barEndInPixels.toInt(), canvas.height)
            } else {
                Rect(barEndInPixels.toInt(), 0, barStartInPixels.toInt(), canvas.height)
            }

            foreground.bounds = barRect
            color?.also {
                foreground.colorFilter = PorterDuffColorFilter(it, PorterDuff.Mode.SRC)
            }
            foreground.draw(this)

            val calibrationStartPaint = if (barRect.contains(calibrationStartInPixels.toInt(), 0)) {
                whiteLine
            } else {
                blackLine
            }

            canvas.drawLine(
                calibrationStartInPixels,
                0f,
                calibrationStartInPixels,
                canvas.height.toFloat(),
                calibrationStartPaint
            )

            val calibrationEndPaint = if (barRect.contains(calibrationEndInPixels.toInt(), 0)) {
                whiteLine
            } else {
                blackLine
            }

            canvas.drawLine(
                calibrationEndInPixels,
                0f,
                calibrationEndInPixels,
                canvas.height.toFloat(),
                calibrationEndPaint
            )
        }
    }

    fun close() {
        surfaceTexture?.let {
            drawThread?.textureRelease = it
            drawThread?.join()
            drawThread = null
        }
    }

    inner class DrawThread() : Thread() {
        var textureRelease: SurfaceTexture? = null

        init {
            priority = (Thread.NORM_PRIORITY + Thread.MAX_PRIORITY) / 2
        }

        override fun run() {
            post { visibility = View.VISIBLE }
            while (textureRelease == null) {
                val currentTime = System.currentTimeMillis()
                val currentProgress = progress?.invoke() ?: lastProgress
                val currentTracking = tracking?.invoke() ?: lastTracking

                if (lastProgress == currentProgress && currentTracking == lastTracking) {
                    sleep(drawInterval)
                    continue
                }
                lastProgress = currentProgress
                lastTracking = currentTracking

                val canvas = this@CheapProgressBar.lockCanvas()
                if (textureRelease != null) {
                    break
                }
                try {
                    canvas?.let {
                        actualDraw(it)
                    }
                } catch (e: Throwable) {
                    e.printStackTrace()
                } finally {
                    if (canvas != null) {
                        this@CheapProgressBar.unlockCanvasAndPost(canvas)
                    }
                }

                val frameTime = System.currentTimeMillis() - currentTime
                sleep(if (frameTime >= drawInterval) 0 else drawInterval - frameTime)
            }
            textureRelease?.release()
        }
    }
}
