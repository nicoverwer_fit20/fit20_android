package fit20.app.di

import fit20.app.presentation.qalityScore.QualityScoreViewModel
import fit20.app.presentation.sideMenu.SideMenuViewModel
import fit20.app.presentation.standby.StandbyViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * *ViewModels* providers\
 *
 * @author a.kokuliuk
 * @since v0.0
 */
object ViewModelModule {
    fun module() = module {
        viewModel { QualityScoreViewModel(get()) }
        viewModel { SideMenuViewModel(get(), get()) }
        viewModel { StandbyViewModel(get(), get(), get()) }
    }
}
