package fit20.app.di

import fit20.app.appState.AppStateInteractor
import fit20.app.appState.AppStateInteractorImpl
import fit20.app.exercise.Exercise
import fit20.app.exercise.ExerciseImpl
import fit20.app.utils.AppUpdater
import org.koin.dsl.module

object DomainModule {
    fun module() = module {
        /**
         * TODO:
         * get is generic method that will look into Koin framework and return dependency if it is exist in the dependency graph
         * single<Exercise> { ExerciseImpl(get()) }
         */
        single<Exercise> { ExerciseImpl }
        single<AppStateInteractor> { AppStateInteractorImpl(get()) }
        single<AppUpdater> { AppUpdater(get(), "https://mscreen-dot-fit20app-4-moduletest.appspot.com/machine-screen-update.xml") }
    }
}
