package fit20.app.di

import fit20.app.data.EncoderDataSource
import fit20.app.data.bleEncoder.BleEncoderDataSource
import fit20.app.data.usbEncoder.UsbEncoderDataSource
import fit20.app.data.usbEncoder.UsbPermissionBroadcastReceiver
import fit20.app.data.wrapper.WrapperEncoderDataSource
import fit20.app.exercise.ExerciseDriver
import org.koin.dsl.module

object DataModule {
    fun module() = module {
        single<EncoderDataSource> {get<WrapperEncoderDataSource>() }
        single { WrapperEncoderDataSource(get()) }
        single { BleEncoderDataSource(get()) }
        single { UsbEncoderDataSource(get()) }
        single { UsbPermissionBroadcastReceiver() }
        single { ExerciseDriver(get()) }
    }
}
