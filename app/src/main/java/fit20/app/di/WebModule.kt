package fit20.app.di

import fit20.app.webserver.KtorWebServerImpl
import fit20.app.webserver.WebServer
import fit20.app.webserver.components.BrightnessWebComponent
import fit20.app.webserver.components.ExerciseWebComponent
import fit20.app.webserver.components.IdentifyWebComponent
import fit20.app.webserver.components.SocketComponent
import org.koin.dsl.module

/**
 * Web server providers
 *
 * Use this class to register your @see [fit20.app.webserver.components.BaseWebComponent]
 *
 * @author a.kokuliuk
 * @since v0.0
 */
object WebModule {
    fun module() = module {
        single<WebServer> {
            KtorWebServerImpl(
                setOf(
                    get<ExerciseWebComponent>(),
                    get<SocketComponent>(),
                    get<BrightnessWebComponent>(),
                    get<IdentifyWebComponent>()
                )
            )
        }
        factory<ExerciseWebComponent> { ExerciseWebComponent(get(), get(), get()) }
        factory<BrightnessWebComponent> { BrightnessWebComponent(get()) }
        factory<IdentifyWebComponent> { IdentifyWebComponent(get()) }
        factory<SocketComponent> { SocketComponent(get(), get(), get()) }
    }
}
