package fit20.app.di

import fit20.app.data.usbEncoder.UsbEncoderDataSource
import fit20.app.utils.IntentListener
import fit20.app.utils.LifecycleObserversHolder
import org.koin.dsl.module

object AndroidModule {
    fun module() = module {
        single {
            IntentListener.IntentListenersHolder(
                setOf(
                    get<UsbEncoderDataSource>()
                )
            )
        }

        single {
            LifecycleObserversHolder(
                setOf(
                    get<UsbEncoderDataSource>()
                )
            )
        }
    }
}