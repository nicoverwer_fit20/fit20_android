package fit20.app.utils

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LifecycleObserver

interface IntentListener {
    fun onNewIntent(context: Context, intent: Intent)

    data class IntentListenersHolder(
        val listeners: Collection<IntentListener>
    )
}

class LifecycleObserversHolder(
    val observers: Collection<LifecycleObserver>
)
