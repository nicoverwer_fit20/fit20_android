package fit20.app.utils

import android.content.Context
import arrow.core.Either
import java.io.IOException
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*


suspend fun getCurrentIpAddress(context: Context): Either<Throwable, String> =
    Either.catch {
        val en: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()
        while (en.hasMoreElements()) {
            val networkInterface: NetworkInterface = en.nextElement()
            val enumIpAddress: Enumeration<InetAddress> = networkInterface.inetAddresses
            while (enumIpAddress.hasMoreElements()) {
                val inetAddress: InetAddress = enumIpAddress.nextElement()
                if (!inetAddress.isLoopbackAddress) {
                    return@catch inetAddress.hostAddress
                }
            }
        }
        throw IOException("Can't find ip address")
    }
