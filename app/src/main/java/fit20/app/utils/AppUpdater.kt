package fit20.app.utils

import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import com.android.volley.Request
import com.android.volley.toolbox.Volley
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root


// https://gist.github.com/itsalif/6149365

class AppUpdater(
    private val context: Context,
    private val url: String
) {
    private val logLabel = "App_Updater_"
    private val queue = Volley.newRequestQueue(context)

    /**
     * Check for an update only once, for example when the memberscreen app is started.
     */
    fun check() : Single<AppUpdate?> {
        Log.i(logLabel, "Check for update at ${url}")
        return Single.create { emitter: SingleEmitter<AppUpdate?> ->
            val request = SimpleXmlRequest(
                Request.Method.GET, url, AppUpdateRoot::class.java,
                { response ->
                    val latestVersionCode = response.update.latestVersionCode ?:0
                    val latestVersion = response.update.latestVersion
                    Log.i(logLabel, "latest versionCode = ${latestVersionCode} version = ${latestVersion}")
                    if (latestVersionCode > 0 && latestVersionCode > getAppInstalledVersionCode()) {
                        Log.i(logLabel, "New version is available according to versionCode")
                        emitter.onSuccess(response.update)
                    } else if (latestVersion != "" && latestVersion > getAppInstalledVersion()) {
                        Log.i(logLabel, "New version is available according to version")
                        emitter.onSuccess(response.update)
                    } else {
                        Log.i(logLabel, "No new version is available")
                        emitter.onSuccess(null)
                    }
                },
                { error ->
                    Log.e(logLabel, "Error received: ${error.message}")
                    emitter.onError(error)
                }
            )
            request.setShouldCache(false)
            queue.cache.clear()
            queue.add(request)
        };
    }

    fun getAppInstalledVersion(): String {
        var version: String = ""
        try {
            version = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        Log.i(logLabel, "Installed version = ${version}")
        return version
    }

    fun getAppInstalledVersionCode(): Int {
        var versionCode = 0
        try {
            versionCode = context.packageManager.getPackageInfo(context.packageName, 0).versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        Log.i(logLabel, "Installed versionCode = ${versionCode}")
        return versionCode
    }
}

// @see https://stackoverflow.com/questions/40487944/parsing-xml-kotlin-android

@Root(name = "AppUpdater", strict = false)
class AppUpdateRoot(
    @field:Element(name = "update", required = true)
    @param:Element(name = "update", required = true)
    val update: AppUpdate
) {
}

class AppUpdate(
    @field:Element(name = "latestVersion", required = true)
    @param:Element(name = "latestVersion", required = true)
    val latestVersion: String,
    @field:Element(name = "latestVersionCode", required = false)
    @param:Element(name = "latestVersionCode", required = false)
    val latestVersionCode: Int? = null,
    @field:Element(name = "url", required = true)
    @param:Element(name = "url", required = true)
    val url: String,
    @field:Element(name = "releaseNotes", required = false)
    @param:Element(name = "releaseNotes", required = false)
    val releaseNotes: String? = null
) {
    // If latestVersionCode is included, latestVersion will only be used to display the latest version in the dialog and the versionCode will be used to compare the installed and latest update.
}
