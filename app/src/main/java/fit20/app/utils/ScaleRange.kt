package fit20.app.utils

/**
 * Value scaling helper method
 *
 * @param toMin target minimal value
 * @param toMax target maximal value
 * @param fromMin actual minimal value
 * @param fromMax actual maximal value
 * @param value value to scale
 * @return scaled value
 *
 * @author a.kokuliuk
 * @since v0.0
 */
fun scaleRange(
    toMin: Double,
    toMax: Double,
    fromMin: Double,
    fromMax: Double,
    value: Double
): Double =
    (((toMax - toMin) * (value - fromMin)) / (fromMax - fromMin)) + toMin
