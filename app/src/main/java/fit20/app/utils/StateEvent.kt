package fit20.app.utils

/**
 * Event which should be handled only once
 *
 * @param value data
 * @property isHandled represents "is processed" flag
 *
 * @author a.kokuliuk
 * @since v0.0
 */
class StateEvent<T>(val value: T) {
    var isHandled: Boolean = false
}

/**
 * StateEvent handle helper method
 *
 * @param block handler lambda, which should return true if handle operation is successful
 *
 * @author a.kokuliuk
 * @since v0.0
 */
inline fun <T> StateEvent<T>?.handle(crossinline block: (T) -> Boolean) {
    this?.apply {
        if (isHandled) {
            isHandled = block(value)
        }
    }
}
