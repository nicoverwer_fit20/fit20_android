package fit20.app.exercise

/**
 * Raw sensor reading data, which is just a signed long integer.
 */
typealias SensorReading = Long

/**
 * Normalized sensor reading data, relative to resting position.
 */
typealias SensorPosition = Long

/**
 * The sensor position used for display and calculations,
 * calibrated so that start-calibration is at 0.0, and end-calibration is at 1.0.
 */
typealias CalibratedPosition = Double

/**
 * Type to make milliseconds explicit.
 */
typealias Milliseconds = Long

/**
 * Data needed for exercise configuration.
 * See the technical spec for the meaning of these properties.
 */
class ExerciseConfiguration (
    // val member: Member, // Not needed for Exercise
    val calibration: Calibration,
    val trackingFactor: Double,
    val easingFactor: Double,
    // val colorScheme: TrackingColors, // Not needed for Exercise
    val underflow : Double, // Not needed for Exercise, maybe later
    val overflow : Double // Not needed for Exercise maybe later
)

/**
 * Calibration consists of a start and end position.
 */
class Calibration (
    val start: SensorPosition,
    val end: SensorPosition
) {
    /**
     * Transform a sensor position to a calibrated position.
     */
    fun calibrated(position: SensorPosition) : CalibratedPosition {
        return if (start == end) 0.0 else (position - start).toDouble() / (end - start)
    }
}

/**
 * Exercise data provided to the outside world.
 * See the technical spec for the meaning of these properties.
 * The count property is null when no counting is shown.
 */
data class ExerciseData(
    val count: Int?,
    val actualPosition: CalibratedPosition,
    val referencePosition: CalibratedPosition,
    val halfRepetition: Int,
    val tracking: Double,
    val time: Milliseconds
)
