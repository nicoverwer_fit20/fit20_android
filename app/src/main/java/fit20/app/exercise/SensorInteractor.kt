package fit20.app.exercise

/**
 * This specifies how raw sensor data is received.
 */
interface SensorInteractor {
    /**
     * Callback function for reporting a changed sensor reading.
     */
    fun onSensorReadingChanged(reading: SensorReading)

    /**
     * Set the callback function for reset.
     * The resetter returns true if the reset succeeded.
     */
    fun setResetter(resetter: () -> Boolean)
}
