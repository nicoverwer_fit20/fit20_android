package fit20.app.exercise

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QualityScore(
    val tempo: Int,
    val rhythm: Int,
    val range: Int,
    val total: Int
) : Parcelable
