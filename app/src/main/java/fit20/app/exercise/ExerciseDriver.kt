package fit20.app.exercise

import android.util.Log
import fit20.app.data.EncoderDataSource

/**
 * Connection layer between encoder data source and exercise
 *
 * @author a.kokuliuk
 * @since v2.0
 */
class ExerciseDriver(
    private val encoderDataSource: EncoderDataSource
) {
    fun init() {
        encoderDataSource.getObservable()
            .onBackpressureLatest()
            .subscribe {
                ExerciseImpl.onSensorReadingChanged(it)
            }
        ExerciseImpl.setResetter {
            Log.i("Exercise", "Using the real resetter!")
            encoderDataSource.resetToZero()
        }
    }
}
