package fit20.app.exercise

data class MachineIdentity(
    val machineId: String,
    val machineName: String,
    val machineTag: String?,
    val sensorTag: String?,
    var sensorMAC: String?
)
