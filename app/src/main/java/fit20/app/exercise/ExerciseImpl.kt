package fit20.app.exercise

import android.util.Log
import kotlin.math.abs
import kotlin.math.max

/**
 * Implementation of business logic.
 */
object ExerciseImpl : Exercise, SensorInteractor {

    private enum class ExerciseState {
        CONFIGURED, STARTED, STATIONARY, STOPPED
    }

    // The time of the count-down after start() is received, before the actual start of the movement.
    private const val countInMilliseconds = 3000 // May be parameterized later.

    // The default configuration to use until configure() is called.
    private val defaultConfiguration = ExerciseConfiguration(
        Calibration(0, 0), 10.0, 1.5, 0.1, 0.1
    )

    // Function to reset the sensor, default does nothing.
    private var resetter = {true}

    private var currentSensorReading: SensorReading = 0
    private var sensorRestingPosition: SensorReading = 0
    private var configuration: ExerciseConfiguration = defaultConfiguration
    // The time of the actual start of the movement, after the count-down.
    private var startTime: Milliseconds = 0
    // The time when the exercise became stationary.
    private var stationaryTime: Milliseconds = 0
    // The initial state is neutral (STOPPED).
    private var state : ExerciseState = ExerciseState.STOPPED
    // The referencePosition is also used and stable during stationary.
    private var referencePosition: CalibratedPosition = 0.0

    override fun getPosition(): SensorPosition {
        return currentSensorReading - sensorRestingPosition
    }

    override fun configure(configuration: ExerciseConfiguration) {
        // Only when entering a new exercise reset the sensor and determine the resting position.
        if (state == ExerciseState.STOPPED) {
            //resetter() // This made the Core sensor crash.
            sensorRestingPosition = currentSensorReading
            Log.i("Exercise", "Sensor resting position is $sensorRestingPosition")
            state = ExerciseState.CONFIGURED
        }
        this.configuration = configuration
    }

    override fun start() {
        startTime = System.currentTimeMillis() + countInMilliseconds
        state = ExerciseState.STARTED
    }

    override fun stationary() {
        stationaryTime = System.currentTimeMillis()
        state = ExerciseState.STATIONARY
    }

    override fun stop() {
        state = ExerciseState.STOPPED
    }

    override fun finish() {
        // Don't keep an old configuration (possibly belonging to someone else).
        // This means that the actual position will be 0.0 until you configure.
        configuration = defaultConfiguration
        state = ExerciseState.STOPPED
        Log.i("Exercise", "Sensor exercise is stopped.")
    }

    override fun getExerciseData(): ExerciseData {
        // During the initial count-down, duration is negative.
        val duration : Milliseconds = System.currentTimeMillis() - startTime
        // durationSec switches to -x at duration = -x000, not at -x001, therefore subtract 999
        val durationSec = if (duration <= 0) (duration - 999) / 1000 else duration / 1000
        val count = when (state) {
            ExerciseState.STOPPED -> null
            ExerciseState.CONFIGURED -> null
            ExerciseState.STARTED -> if (durationSec <= 0) durationSec.toInt() % 10 else (durationSec.toInt() - 1) % 10 + 1
            ExerciseState.STATIONARY -> max(0, 10 - ((System.currentTimeMillis() - stationaryTime) / 1000).toInt())
        }
        val actualPosition = configuration.calibration.calibrated(getPosition())
        // Only change reference position when exercise is ongoing.
        referencePosition = when (state) {
            ExerciseState.STOPPED -> 0.0
            ExerciseState.CONFIGURED -> 0.0
            ExerciseState.STARTED -> referencePosition(duration)
            ExerciseState.STATIONARY -> referencePosition // unchanged
        }
        val halfRepetition =
            when (state) {
                ExerciseState.STARTED -> if (duration < 0) 0 else (duration / 10000).toInt() + 1
                ExerciseState.STATIONARY -> -1
                else -> 0
            }
        val tracking = configuration.trackingFactor * abs(actualPosition - referencePosition)
        return ExerciseData(
            count = count,
            actualPosition = actualPosition,
            referencePosition = referencePosition,
            halfRepetition = halfRepetition,
            tracking = tracking,
            time = duration
        )
    }

    private fun referencePosition(duration: Milliseconds) : CalibratedPosition {
        if (duration < 0) return 0.0
        val repDurationSec = (duration.toDouble() / 1000) % 20
        val d = if (repDurationSec < 10) repDurationSec else 20 - repDurationSec
        val s = configuration.easingFactor
        return when {
            (s <= 0.0 || s >= 10.0)   ->  0.0
            d < s                     ->  (d * d / 2) / ((10 - s) * s)
            d <= 10 - s               ->  (s * (d - s / 2)) / ((10 - s) * s)
            d > 10 - s                ->  (s * (10 - s) - (10 - d) * (10 - d) / 2) / ((10 - s) * s)
            else                      ->  {Log.e("Exercise", "referencePosition could not be computed at d = $duration"); referencePosition}
        }
    }

    override fun onSensorReadingChanged(reading: SensorReading) {
        currentSensorReading = reading
    }

    override fun setResetter(resetter: () -> Boolean) {
        this.resetter = resetter
    }

}
