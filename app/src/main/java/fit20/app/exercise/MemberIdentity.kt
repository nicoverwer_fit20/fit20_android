package fit20.app.exercise

data class MemberIdentity(
    val id: String,
    val name: String
)