package fit20.app.exercise

/**
 * Specification of the business logic for an exercise.
 * This part receives commands (configure, start, ...) and
 * provides normalized and calibrated positons, as well as other exercise data.
 */
interface Exercise {

    /**
     * Get the sensor position, relative to the resting position.
     */
    fun getPosition(): SensorPosition

    /**
     * Set the configuration of the exercise.
     * When configure is called, the sensor resting position is set.
     * Therefore, at least one sensor reading must have been sent to the Exercise.
     * This function is also used to set the calibration.
     */
    fun configure(configuration: ExerciseConfiguration)

    /**
     * Start the exercise.
     * This will first do the count-down before the actual movement starts.
     */
    fun start()

    /**
     * Enter the stationary state of the exercise.
     */
    fun stationary()

    /**
     * Stop the exercise (in the UI, this shows the result).
     */
    fun stop()

    /**
     * Finish the exercise (in the UI, this clears the screen).
     */
    fun finish()

    /**
     * Get the current exercise data.
     */
    fun getExerciseData(): ExerciseData
}
