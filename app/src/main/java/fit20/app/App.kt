package fit20.app

import android.app.Application
import fit20.app.appState.AppStateInteractor
import fit20.app.di.*
import fit20.app.exercise.ExerciseDriver
import fit20.app.webserver.WebServer
import fit20.app.webserver.components.NetworkServiceDiscovery
import kotlinx.coroutines.newSingleThreadContext
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

/**
 * Fit20 application
 *
 * Do magic here!
 *
 * @author a.kokuliuk
 * @since v0.0
 */
class App : Application() {

    private val webServer: WebServer by inject()
    private val exerciseDriver: ExerciseDriver by inject()

    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin {
            androidContext(this@App)
            modules(
                module {
                    single<Cicerone<Router>> { Cicerone.create(Router()) }
                    single<NavigatorHolder> { get<Cicerone<Router>>().navigatorHolder }
                    single<Router> { get<Cicerone<Router>>().router }
                },
                AndroidModule.module(),
                WebModule.module(),
                DataModule.module(),
                DomainModule.module(),
                ViewModelModule.module()
            )
        }
        webServer.start()
        exerciseDriver.init()
        NetworkServiceDiscovery()

    }

    companion object {
        lateinit var instance: App
            private set
        val restDispatcher = newSingleThreadContext("REST")
    }
}
