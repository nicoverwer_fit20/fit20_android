package fit20.app.exceptions

/**
 * Exception which represents unsuccessful attempt to mutate application state
 *
 * @author a.kokuliuk
 * @since v0.0
 */
sealed class ExerciseException : Exception()
sealed class ExerciseStateException : ExerciseException() {
    class AnotherActiveExerciseExistsException : ExerciseStateException()
    class ActiveExerciseNotFound : ExerciseStateException()
}
