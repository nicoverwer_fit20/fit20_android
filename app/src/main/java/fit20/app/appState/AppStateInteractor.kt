package fit20.app.appState

import android.content.Context
import fit20.app.exercise.MachineIdentity
import fit20.app.exercise.MemberIdentity
import fit20.app.exercise.QualityScore
import io.reactivex.rxjava3.core.Flowable

/**
 * Application state interactor
 *
 * @use [AppStateInteractor] for navigation inside the app
 *
 * @author a.kokuliuk
 * @since v0.0
 */
interface AppStateInteractor {

    /**
     * Get the application context
     */
    fun getContext(): Context

    /**
     * Set new app state
     *
     * @since v2.0
     */
    fun newState(state: AppState)

    /**
     * Returns state, which is basically current screen
     *
     * @since v0.0
     */
    fun getAppState(): Flowable<AppState>

    /**
     * Sets screen brightness
     *
     * @param brightness screen brightness from 0 (dark) to 255 (bright)
     *
     * @since v2.0
     */
    fun setBrightness(brightness: Int)

    /**
     * Returns screen brightness observable
     *
     * @return Flowable<Int> screen brightness from 0 (dark) to 255 (bright)
     *
     * @since v2.0
     */
    fun getBrightness(): Flowable<Int>

    /**
     * Sets Machine Identity
     *
     * @param machineIdentity the identity of machine
     *
     * @author b.savrasov
     * @since v2.0
     */
    fun setMachineIdentity(machineIdentity: MachineIdentity)

    /**
     * Returns machine identity
     *
     * @return Flowable<MachineIdentity> flow of machineIdentity
     *
     * @author b.savrasov
     * @since v2.0
     */
    fun getMachineIdentity(): Flowable<MachineIdentity>

    /**
     * Sets Member Identity
     *
     * @param memberIdentity the identity of member
     *
     * @author b.savrasov
     * @since v2.0
     */
    fun setMemberIdentity(memberIdentity: MemberIdentity)

    /**
     * Returns member identity
     *
     * @return Flowable<MemberIdentity> flow of member identity
     *
     * @author b.savrasov
     * @since v2.0
     */
    fun getMemberIdentity(): Flowable<MemberIdentity>

    /**
     * Sets Color Scheme
     *
     * @param colorScheme color scheme of bottom progress bar
     *
     * @author b.savrasov
     * @since v2.0
     */
    fun setColorScheme(colorScheme: Map<Float, Int>)

    /**
     * Returns color scheme flow
     *
     * @return Flowable<Map<Float, Int>> of color scheme of bottom progress bar
     *
     * @author b.savrasov
     * @since v2.0
     */
    fun getColorScheme(): Flowable<Map<Float, Int>>

    /**
     * Class that represents application state
     *
     * @author a.kokuliuk
     * @since v0.0
     */
    sealed class AppState {
        object StandbyState : AppState()
        object ExerciseState : AppState()
        data class QualityScoreState(val qualityScore: QualityScore? = null) : AppState()
    }
}
