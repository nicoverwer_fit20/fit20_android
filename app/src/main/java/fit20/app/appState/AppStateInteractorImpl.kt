package fit20.app.appState

import android.content.Context
import android.graphics.Color
import fit20.app.R
import fit20.app.exercise.MachineIdentity
import fit20.app.exercise.MemberIdentity
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subjects.BehaviorSubject

/**
 * @see [AppStateInteractor] implementation
 *
 * @author a.kokuliuk
 * @since v0.0
 */
class AppStateInteractorImpl(
    private val context: Context
) : AppStateInteractor {

    private val state =
        BehaviorSubject.createDefault(AppStateInteractor.AppState.StandbyState as AppStateInteractor.AppState)

    private val brightness = BehaviorSubject.create<Int>()

    private val machineIdentity = BehaviorSubject.create<MachineIdentity>()
    private val memberIdentity = BehaviorSubject.create<MemberIdentity>()
    private val colorScheme = BehaviorSubject.create<Map<Float, Int>>()

    init {
        loadMachineIdentity()?.also {
            machineIdentity.onNext(it)
        }

        colorScheme.onNext(
            mapOf(
                1f to Color.parseColor("#97c332"),
                1.5f to Color.parseColor("#de792e"),
                2f to Color.parseColor("#d40000")
            )
        )
    }

    override fun getContext(): Context = context

    override fun getAppState(): Flowable<AppStateInteractor.AppState> =
        state.toFlowable(BackpressureStrategy.LATEST)

    override fun newState(state: AppStateInteractor.AppState) {
        this.state.onNext(state)
    }

    override fun setBrightness(brightness: Int) {
        this.brightness.onNext(brightness)
    }

    override fun getBrightness(): Flowable<Int> =
        brightness.toFlowable(BackpressureStrategy.LATEST)

    override fun setMachineIdentity(machineIdentity: MachineIdentity) {
        this.machineIdentity.onNext(machineIdentity)
        storeMachineIdentity(machineIdentity)
    }

    override fun getMachineIdentity(): Flowable<MachineIdentity> =
        machineIdentity.toFlowable(BackpressureStrategy.LATEST)

    override fun setMemberIdentity(memberIdentity: MemberIdentity) {
        this.memberIdentity.onNext(memberIdentity)
    }

    override fun getMemberIdentity(): Flowable<MemberIdentity> =
        memberIdentity.toFlowable(BackpressureStrategy.LATEST)

    override fun setColorScheme(colorScheme: Map<Float, Int>) {
        this.colorScheme.onNext(colorScheme)
    }

    override fun getColorScheme(): Flowable<Map<Float, Int>> =
        colorScheme.toFlowable(BackpressureStrategy.LATEST)

    private fun loadMachineIdentity(): MachineIdentity? {
        val appName = context.getString(R.string.app_name)
        val prefs = context.getSharedPreferences(appName, Context.MODE_PRIVATE)
        val id = prefs.getString("machineId", null)
        val name = prefs.getString("machineName", null)
        val machineTag = prefs.getString("machineTag", "")
        val sensorTag = prefs.getString("sensorTag", null)
        val sensorMAC = prefs.getString("sensorMAC", null)
        return MachineIdentity(id ?: "?", name ?: "?", machineTag, sensorTag, sensorMAC)
    }

    private fun storeMachineIdentity(machineIdentity: MachineIdentity) {
        val appName = context.getString(R.string.app_name)
        val prefs = context.getSharedPreferences(appName, Context.MODE_PRIVATE)
        with (prefs.edit()) {
            putString("machineId", machineIdentity.machineId)
            putString("machineName", machineIdentity.machineName)
            putString("machineTag", machineIdentity.machineTag)
            putString("sensorTag", machineIdentity.sensorTag)
            putString("sensorMAC", machineIdentity.sensorMAC)
            apply()
        }
    }
}
