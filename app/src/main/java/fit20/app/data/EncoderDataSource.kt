package fit20.app.data

import fit20.app.exercise.SensorReading
import io.reactivex.rxjava3.core.Flowable

/**
 * Encoder data source
 *
 * @author a.kokuliuk
 * @since v2.0
 */
interface EncoderDataSource {
    /**
     * Last known sensor position
     *
     * @return last known position
     *
     * @since v2.0
     */
    fun getLastKnownReading(): SensorReading

    /**
     * Encoder position observable
     *
     * @return encoder position observable
     *
     * @since 2.0
     */
    fun getObservable(): Flowable<SensorReading>

    /**
     * Return data source connection state
     *
     * @return [EncoderConnectionState] observable
     *
     * @since v2.0
     */
    fun getConnectionStateObservable(): Flowable<EncoderConnectionState>

    /**
     * Reset encoder to zero position
     *
     * @return true if reset operation successful, false - if not
     *
     * @since v2.0
     */
    fun resetToZero(): Boolean

    /**
     * Data source connection state
     *
     * @author a.kokuliuk
     * @since v2.0
     */
    sealed class EncoderConnectionState {
        open class ConnectedInfo(private val info: String) : EncoderConnectionState(){
            override fun toString() = info
        }
        object Connected : EncoderConnectionState.ConnectedInfo("connected")
        open class DisconnectedInfo(private val info: String) : EncoderConnectionState(){
            override fun toString() = info
        }
        object Disconnected : EncoderConnectionState.DisconnectedInfo("disconnected")
        class Error(val t: Throwable) : EncoderConnectionState(){
            override fun toString() = "error: ${t.message}"
        }
        fun isConnected() : Boolean {
            return this is ConnectedInfo || this is Connected
        }
    }
}
