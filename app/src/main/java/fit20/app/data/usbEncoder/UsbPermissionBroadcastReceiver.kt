package fit20.app.data.usbEncoder

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.hardware.usb.UsbAccessory
import android.hardware.usb.UsbManager
import android.util.Log
import kotlinx.coroutines.runBlocking
import org.koin.core.KoinComponent
import org.koin.core.inject


class UsbPermissionBroadcastReceiver : BroadcastReceiver(), KoinComponent {

    private val encoderDataSource: UsbEncoderDataSource by inject()

    override fun onReceive(context: Context, intent: Intent) {
        Log.d("USB_E_", intent.toString())
        if (intent.action == ACTION_USB_PERMISSION) {
            synchronized(this) {
                val accessory: UsbAccessory? = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY)
                if (intent.getBooleanExtra(
                        UsbManager.EXTRA_PERMISSION_GRANTED,
                        false
                    ) && accessory != null
                ) {
                    runBlocking {
                        encoderDataSource.tryEnable().fold({
                            throw it
                        }, {
                            Log.d("USB_E_", "Connected");
                        })
                    }
                } else {
                    Log.d("USB_E_", "permission denied for accessory $accessory");
                }
            }
        }
    }

    companion object {
        const val ACTION_USB_PERMISSION = "fit20.app.USB_PERMISSION";
    }
}
