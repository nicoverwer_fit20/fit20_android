package fit20.app.data.usbEncoder

import android.content.Context
import android.content.Intent
import android.hardware.usb.UsbManager
import android.os.Handler
import android.os.Message
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import arrow.core.Either
import arrow.core.left
import arrow.core.right
import fit20.app.data.EncoderDataSource
import fit20.app.data.usbutils.USBAccessoryManager
import fit20.app.data.usbutils.USBAccessoryManagerMessage
import fit20.app.data.usbutils.postSuspendable
import fit20.app.exercise.SensorReading
import fit20.app.utils.IntentListener
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.subjects.BehaviorSubject
import kotlinx.coroutines.*
import java.io.IOException
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.math.abs

/**
 * @see [EncoderDataSource] Microchip implementation
 *
 * Usb encoder data source
 *
 * @author a.kokuliuk
 * @since v2.0
 */
class UsbEncoderDataSource(
    private val context: Context
) : EncoderDataSource, LifecycleObserver, IntentListener, CoroutineScope {
    private var resetSensorWatcher: CancellableContinuation<Boolean>? = null
    private var shouldBeConnected = BehaviorSubject.createDefault(false)
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default

    /** Last reading*/
    private var lastReading: Long = 0
        set(value) {
            field = value
            readingObservable.onNext(value)
        }
    private val connectionStateObservable =
        BehaviorSubject.create<EncoderDataSource.EncoderConnectionState>()

    /** Connection state */
    private var connectionState: EncoderDataSource.EncoderConnectionState =
        EncoderDataSource.EncoderConnectionState.Disconnected
        private set(value) {
            field = value
            Log.d("USB_E_", "New state: $connectionState")
            connectionStateObservable.onNext(value)
        }
    private val readingObservable =
        BehaviorSubject.create<SensorReading>()

    private val handler: SensorHandler by lazy {
        runBlocking {
            withContext(Dispatchers.Main) {
                SensorHandler(
                    context, {
                        accessoryManager!!
                    }, {
                        lastReading = it
                        if (resetSensorWatcher?.isActive == true && abs(it) < 10) {
                            resetSensorWatcher?.resume(true)
                        }
                    }, {
                        connectionState = it
                    })
            }
        }
    }

    init {
        connectionStateObservable
            .subscribe {
                Log.d("USB_E_", "Retry connection $it")
                launch {
                    tryEnable()
                }
            }

        Observable.combineLatest(
            connectionStateObservable,
            shouldBeConnected,
            BiFunction<EncoderDataSource.EncoderConnectionState, Boolean, Boolean> { t1, t2 ->
                t1 != EncoderDataSource.EncoderConnectionState.Connected && t2
            })
            .delay(3, TimeUnit.SECONDS)
            .subscribe {
                launch { tryEnable() }
            }

    }

    private var accessoryManager: USBAccessoryManager<SensorHandler>? = null
        get() {
            if (field == null) {
                val manager = USBAccessoryManager(
                    handler,
                    SensorHandler.USBAccessoryWhat
                )
                field = manager
                return manager
            }
            return field
        }

    override fun onNewIntent(context: Context, intent: Intent) {
        Log.d("USB_E_", "New intent $intent")
        launch {
            when (intent.action) {
                UsbManager.ACTION_USB_ACCESSORY_ATTACHED -> enable()
                UsbManager.ACTION_USB_ACCESSORY_DETACHED -> disable()
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        launch {
            enable()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onPause() {
        launch {
            disable()
        }
    }

    private suspend fun enable() {
        shouldBeConnected.onNext(true)
        tryEnable().fold({
            it.printStackTrace()
            Log.d("USB_E_", it.message ?: "Unknown")
            connectionState = EncoderDataSource.EncoderConnectionState.Error(it)
        }, {
            connectionState = EncoderDataSource.EncoderConnectionState.Connected
            Log.d("USB_E_", "Connected!")
        })
    }

    suspend fun tryEnable(): Either<Throwable, Unit> =
        if (shouldBeConnected.value) {
            handler.postSuspendable {
                synchronized(this) {
                    Log.d("USB_E_", "Connect")
                    val r = if (accessoryManager?.isConnected == false) {
                        when (val result = accessoryManager?.enable(context, null)) {
                            USBAccessoryManager.RETURN_CODES.SUCCESS ->
                                Unit.right()
                            null -> RuntimeException("AccessoryManager.enable() == null").left()
                            else -> IOException(result.toString()).left()
                        }
                    } else {
                        Unit.right()
                    }
                    r
                }
            }
        } else {
            Unit.right()
        }

    private fun disable() {
        shouldBeConnected.onNext(false)
        launch {
            Log.d("USB_E_", "Disable")
            handler.postSuspendable {
                accessoryManager?.handler?.disable()
                accessoryManager = null
            }
        }
    }

    override fun getLastKnownReading(): SensorReading = lastReading

    override fun getObservable(): Flowable<SensorReading> =
        readingObservable.toFlowable(BackpressureStrategy.LATEST)

    override fun getConnectionStateObservable(): Flowable<EncoderDataSource.EncoderConnectionState> =
        connectionStateObservable.toFlowable(BackpressureStrategy.LATEST)

    private suspend fun sendResetCommand(): Boolean = suspendCancellableCoroutine<Boolean> {
        handler.post {
            try {
                if (accessoryManager?.isConnected == true) {
                    accessoryManager?.write(byteArrayOf(7, 0, 0))
                    it.resume(true)
                } else {
                    it.resume(false)
                }
            } catch (t: Throwable) {
                Log.d("USB_ER_", t.message.orEmpty())
                t.printStackTrace()
                it.resume(false)
            }
        }
    }

    override fun resetToZero(): Boolean =
        runBlocking {
            launch {
                delay(1000)
                if (resetSensorWatcher?.isActive == true) {
                    throw IOException("Sensor reset timeout")
                }
            }
            sendResetCommand()
            suspendCancellableCoroutine<Boolean> { continuation ->
                resetSensorWatcher = continuation
            }
        }
}

internal class SensorHandler(
    private val context: Context,
    private val accessoryManager: () -> USBAccessoryManager<SensorHandler>,
    private val onNewValueReceived: (SensorReading) -> Unit,
    private val onNewState: (EncoderDataSource.EncoderConnectionState) -> Unit
) : Handler() {
    var firmwareProtocol = 0
    var deviceAttached = false
    override fun handleMessage(msg: Message?) {
        if (msg == null) return
        when (msg.what) {
            USBAccessoryWhat -> {
                val msgObj = msg.obj as USBAccessoryManagerMessage
                when (msgObj.type) {
                    USBAccessoryManagerMessage.MessageType.READ -> {
                        if (!accessoryManager().isConnected) {
                            return
                        }
                        while (accessoryManager().available() >= 2) {
                            // All commands are 2 bytes. If there is 1 byte, that is a partial command.
                            val commandPacket = ByteArray(3)
                            accessoryManager().read(commandPacket)
                            when (commandPacket[0]) {
                                ENCODER_CHANGE -> {
                                    val encoderReading =
                                        commandPacket[2] * 0x100 + (commandPacket[1].toUByte()).toInt()
                                    //Log.d("USB_E_", encoderReading.toString())
                                    onNewValueReceived.invoke(encoderReading.toLong())
                                }
                                else -> {
                                }
                            }
                        }
                    }
                    USBAccessoryManagerMessage.MessageType.READY -> {
                        val version = msgObj.accessory.version ?: "0"
                        firmwareProtocol = version.substringBefore('.').toInt()
                        deviceAttached = true
                        if (firmwareProtocol > 1) {
                            accessoryManager().write(
                                byteArrayOf(
                                    APP_CONNECT,
                                    0,
                                    0
                                )
                            )
                        }
                    }
                    USBAccessoryManagerMessage.MessageType.CONNECTED -> {
                        onNewState(EncoderDataSource.EncoderConnectionState.Connected)

                    }
                    USBAccessoryManagerMessage.MessageType.DISCONNECTED -> {
                        onNewState(EncoderDataSource.EncoderConnectionState.Disconnected)

                    }
                    USBAccessoryManagerMessage.MessageType.ERROR -> {
                        onNewState(
                            EncoderDataSource.EncoderConnectionState.Error(
                                IOException()
                            )
                        )
                    }
                    else -> {
                    }
                }
            }
        }
    }

    fun disable() {
        if (firmwareProtocol == 2) {
            accessoryManager().write(byteArrayOf(APP_DISCONNECT, 0, 0))
        }
        accessoryManager().disable(context)
    }

    companion object {
        internal const val USBAccessoryWhat = 0
        private const val ENCODER_CHANGE = 4.toByte()
        private const val APP_CONNECT = 0xFE.toByte()
        private const val APP_DISCONNECT = 0xFF.toByte()
    }
}
