package fit20.app.data.bleEncoder

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Application
import android.bluetooth.BluetoothGatt
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import com.clj.fastble.BleManager
import com.clj.fastble.callback.BleGattCallback
import com.clj.fastble.callback.BleNotifyCallback
import com.clj.fastble.callback.BleScanCallback
import com.clj.fastble.data.BleDevice
import com.clj.fastble.exception.BleException
import com.clj.fastble.exception.ConnectException
import com.clj.fastble.scan.BleScanRuleConfig
import fit20.app.appState.AppStateInteractor
import fit20.app.data.EncoderDataSource
import fit20.app.exercise.MachineIdentity
import fit20.app.exercise.SensorReading
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.nio.ByteBuffer


/**
 * Use the old Isensit sensors as a data source via Bluetooth Low Energy (BLE).
 * If the tablet cannot connect to the sensor, make sure that the app has permissions to use location.
 *
 * Problems occur using old Isensit tablets.
 * See https://stackoverflow.com/questions/48764878/ble-gatt-discoverservices-returns-true-but-callback-never-fires
 *     https://e2e.ti.com/support/wireless-connectivity/bluetooth/f/538/t/414695?CC2541-Android-Connection-Problem
 */
class BleEncoderDataSource(
    private val application: Application
) : EncoderDataSource, KoinComponent {
    private val appState: AppStateInteractor by inject()

    private val SERVICE_UUID = "0000fff0-0000-1000-8000-00805f9b34fb"
    private val SENSOR_CHARACTERISTIC_UUID = "0000fff4-0000-1000-8000-00805f9b34fb"
    // private val DATA_RATE_CHARACTERISTIC_UUID = "0000fff3-0000-1000-8000-00805f9b34fb"

    private val logLabel = "BLE_E_"

    // The machine identity that the tablet belongs to
    private var machineIdentity: MachineIdentity? = null

    // readingOffset is a signed long that is added to the reading when the reading over- or underflows and wraps around.
    private var readingOffset = 0L

    // Are we scanning?
    private var scanning = false;

    // connectedBleDevice is the connected sensor
    private var connectedBleDevice: BleDevice? = null

    // Sometimes, the machine name is known.
    private var machineName: String? = null

    /** Connection state */

    private val connectionStateObservable =
        BehaviorSubject.create<EncoderDataSource.EncoderConnectionState>()

    private var connectionState : EncoderDataSource.EncoderConnectionState =
          EncoderDataSource.EncoderConnectionState.DisconnectedInfo("New sensor connection")
        private set(value) {
            field = value
            val state = if (value.isConnected()) "connected" else "not connected"
            connectionStateObservable.onNext(value)
            Log.i(logLabel, "New state: ${state}; $connectionState")
        }

    /**
     * Return data source connection state
     * @return observable
     */
    override fun getConnectionStateObservable(): Flowable<EncoderDataSource.EncoderConnectionState> =
        connectionStateObservable.toFlowable(BackpressureStrategy.LATEST)

    /**
     * Upon initialization, start scanning for a paired BLE sensor.
     * The connectionState indicates whether this is successful.
     */
    init {
        connectionState = EncoderDataSource.EncoderConnectionState.DisconnectedInfo("Switching to Bluetooth LE")
        appState.getMachineIdentity().subscribe {
            Log.i(logLabel, "Machine identity changed from ${machineIdentity?.machineTag} (${machineIdentity?.sensorTag}) to ${it?.machineTag} (${it?.sensorTag})")
            val mustScanAgain = machineIdentity == null || machineIdentity?.sensorMAC != it?.sensorMAC || machineIdentity?.sensorTag != it?.sensorTag
            machineIdentity = it
            if (mustScanAgain) {
                Log.i(logLabel, "Scanning after change in sensor.")
                scanConnectToDevice(it)
            } else {
                Log.i(logLabel, "Not scanning because sensor did not change.")
            }
        }
    }

    /**
     * Location permissions are needed for using Bluetooth LE.
     */
    fun checkForPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                application, ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED) {
            connectionState = EncoderDataSource.EncoderConnectionState.Error(
                Exception(application.getString(fit20.app.R.string.need_BLE_permission))
            )
//            ActivityCompat.requestPermissions(
//                application.HowDoIGetAnActivity?,
//                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
//                1010
//            )
            return false
        } else {
            return true
        }
    }

    /**
     * Disconnect and clean up all BLE things.
     * Do this when you want to terminate the BleEncoderDataSource.
     */
    public fun disconnect() {
        Log.i(logLabel, "Disconnecting")
        if (scanning) {
            Log.i(logLabel, "Disconnect: Cancel scan")
            BleManager.getInstance().cancelScan();
        }
        if (connectedBleDevice != null) {
            Log.i(logLabel, "Disconnect from ${connectedBleDevice?.name}")
            BleManager.getInstance().disconnect(connectedBleDevice)
        }
        BleManager.getInstance().disconnectAllDevice()
        BleManager.getInstance().destroy()
        Log.i(logLabel, "Disconnected")
    }

    private fun scanConnectToDevice(machineIdentity: MachineIdentity) {
        val sensorMAC = machineIdentity.sensorMAC
        val sensorTag = machineIdentity.sensorTag
        val machineName = machineIdentity.machineName
        this.machineName = machineName
        readingOffset = 0L
        connectionState = EncoderDataSource.EncoderConnectionState.DisconnectedInfo(
            "Scan/Connect: Initializing Bluetooth for ${machineName} sensor ${sensorTag} at ${sensorMAC}."
        )
        disconnect()
        BleManager.getInstance().init(application)
        BleManager.getInstance()
            .enableLog(true)
            .setReConnectCount(20, 5000)
            .setSplitWriteNum(20)
            //.setConnectOverTime(10000)
            .setOperateTimeout(180000)
        if (BleManager.getInstance().isSupportBle) {
            // First try bonded ("paired") devices
            val bondedECDevice = BleManager.getInstance().bluetoothAdapter.bondedDevices.firstOrNull { it.name.startsWith("EC")}
            if (bondedECDevice != null) {
                Log.i(logLabel, "Scan/Connect: Paired sensor, connecting to ${bondedECDevice.name}.")
                connectToDevice(BleDevice(bondedECDevice))
            } else if (sensorMAC != null) {
                Log.i(logLabel, "Scan/Connect: MAC of sensor configured, connecting to ${sensorMAC}.")
                connectToMAC(sensorMAC)
            } else if (sensorTag != null) {
                Log.i(logLabel, "Scan/Connect: Sensor name configured, connecting to ${sensorTag}")
                scanForDevice(sensorTag)
            } else {
                Log.e(logLabel, "Scan/Connect: No paired or configured sensor was found.")
                connectionState = EncoderDataSource.EncoderConnectionState.Error(Exception("There is no Bluetooth sensor configured for ${machineName ?: "unknown machine"}."))
            }
        } else {
            connectionState = EncoderDataSource.EncoderConnectionState.Error(Exception("This tablet device does not support Bluetooth Low Energy (BLE)."))
        }
    }

    private fun scanForDevice(deviceName: String?) {
        var selectedBleDevice: BleDevice? = null
        if (checkForPermissions()) {
            val scanRuleConfig = BleScanRuleConfig.Builder()
                .setAutoConnect(true)
                .setScanTimeOut(300000)
                .setDeviceName(deviceName == null, deviceName ?: "EC")
                .build()
            BleManager.getInstance().initScanRule(scanRuleConfig)
            BleManager.getInstance().scan(object : BleScanCallback() {
                override fun onScanStarted(success: Boolean) {
                    selectedBleDevice = null
                    if (success) {
                        scanning = true
                        connectionState =
                            EncoderDataSource.EncoderConnectionState.DisconnectedInfo("Start scanning for ${machineName ?: ""} sensor ${deviceName ?: ""}")
                        Log.i(logLabel, "Scan: Scan started looking for ${deviceName ?: "EC..."}.")
                    } else {
                        connectionState =
                            EncoderDataSource.EncoderConnectionState.Error(Exception("Bluetooth scan for ${deviceName ?: "EC..."} cannot be started. Check if Bluetooth is enabled on this tablet."))
                    }
                }

                override fun onScanning(bleDevice: BleDevice) {
                    Log.i(logLabel, "Scan: Found ${bleDevice.name}.")
                    selectedBleDevice = bleDevice
                    BleManager.getInstance().cancelScan()
                }

                override fun onScanFinished(scanResultList: List<BleDevice>) {
                    scanning = false;
                    Log.i(logLabel, "Scan: finished scanning for ${deviceName ?: "EC..."}.")
                    if (selectedBleDevice == null) {
                        // No device was found.
                        connectionState =
                            EncoderDataSource.EncoderConnectionState.Error(Exception("Sensor ${deviceName ?: "EC..."} for $machineName not found."))
                        android.os.Handler(application.mainLooper).postDelayed(
                            { scanForDevice(deviceName) },
                            BleManager.getInstance().reConnectInterval
                        )
                    } else {
                        // The device was found, now connect to it.
                        val machId = machineIdentity
                        val selectedMAC = selectedBleDevice?.mac
                        if (selectedMAC != null && machId != null) {
                            // Add the MAC to the machine identity.
                            machId.sensorMAC = selectedMAC
                            appState.setMachineIdentity(machId)
                            Log.i(
                                logLabel,
                                "Scan: Found ${selectedBleDevice?.name}, connecting via MAC ${machId.sensorMAC} for ${machId.machineName} (${machId.machineTag} ${machId.sensorTag})."
                            )
                            connectToMAC(selectedMAC)
                        } else {
                            Log.i(
                                logLabel,
                                "Scan: Found ${selectedBleDevice?.name}, connecting via name."
                            )
                            connectToDevice(selectedBleDevice!!)
                        }
                    }
                }
            }) // scan
        }
    }

    private fun connectToDevice(bleDevice: BleDevice) {
        Log.i(logLabel, "Connect via device name starting.")
        BleManager.getInstance().connect(bleDevice, object : BleGattCallback() {
            override fun onStartConnect() {
                connectedBleDevice = null
                connectionState = EncoderDataSource.EncoderConnectionState.DisconnectedInfo("Connecting to ${machineName} sensor ${bleDevice.name}")
                Log.i(logLabel, "Connect to device: started.")
            }

            override fun onConnectFail(bleDevice: BleDevice, exception: BleException) {
                connectedBleDevice = null
                connectionState = EncoderDataSource.EncoderConnectionState.Error(exception("Connection to sensor failed.", exception))
                Log.e(logLabel, "Connect to device: failed for sensor ${bleDevice.name}, ${exception.description} code=${exception.code}.")
                if (exception is ConnectException) {
                    Log.e(logLabel, " ^^ gattStatus=${exception.gattStatus}")
                }
                android.os.Handler(application.mainLooper).postDelayed(
                    { connectToDevice(bleDevice) },
                    BleManager.getInstance().reConnectInterval
                )
            }

            override fun onConnectSuccess(bleDevice: BleDevice, gatt: BluetoothGatt, status: Int) {
                connectedBleDevice = bleDevice
                connectionState = EncoderDataSource.EncoderConnectionState.ConnectedInfo("${machineName} sensor ${bleDevice.name},\nwaiting for data")
                startNotifying(bleDevice, gatt)
            }

            override fun onDisConnected(isActiveDisConnected: Boolean, bleDevice: BleDevice, gatt: BluetoothGatt, status: Int) {
                connectedBleDevice = null
                val why = if (isActiveDisConnected) "actively" else "spontaneously"
                connectionState = EncoderDataSource.EncoderConnectionState.DisconnectedInfo("${machineName} sensor ${bleDevice.name} $why disconnected")
                Log.w(logLabel, "Connect to device: Disconnected, $why")
                android.os.Handler(application.mainLooper).postDelayed(
                    { connectToDevice(bleDevice) },
                    BleManager.getInstance().reConnectInterval
                )
            }
        })
    }

    private fun connectToMAC(sensorMAC: String) {
        Log.i(logLabel, "Connect via MAC starting.")
        BleManager.getInstance().connect(sensorMAC, object : BleGattCallback() {
            override fun onStartConnect() {
                connectedBleDevice = null
                connectionState = EncoderDataSource.EncoderConnectionState.DisconnectedInfo("Connecting via MAC to ${machineName} sensor.")
                Log.i(logLabel, "Connect via MAC ${sensorMAC}: started.")
            }

            override fun onConnectFail(bleDevice: BleDevice, exception: BleException) {
                connectedBleDevice = null
                connectionState = EncoderDataSource.EncoderConnectionState.Error(exception("Connection to ${machineName} sensor via MAC failed.", exception))
                Log.e(logLabel, "Connect via MAC ${sensorMAC}: failed for sensor ${bleDevice.name}, ${exception.description} code=${exception.code}")
                if (exception is ConnectException) {
                    Log.e(logLabel, " ^^ gattStatus=${exception.gattStatus}")
                }
                // Try to connect via the sensor name (sensorTag) if MAC fails.
                if (machineIdentity?.sensorTag != null) {
                    android.os.Handler(application.mainLooper).postDelayed(
                        { scanForDevice(machineIdentity?.sensorTag) },
                        BleManager.getInstance().reConnectInterval
                    )
                }
            }

            override fun onConnectSuccess(bleDevice: BleDevice, gatt: BluetoothGatt, status: Int) {
                connectedBleDevice = bleDevice
                val name = bleDevice.name ?: gatt.device.name ?: machineIdentity?.sensorTag ?: ""
                connectionState = EncoderDataSource.EncoderConnectionState.ConnectedInfo("${machineName} sensor ${name},\nwaiting for data")
                Log.i(logLabel, "Connected via MAC ${bleDevice.mac}")
                startNotifying(bleDevice, gatt)
            }

            override fun onDisConnected(isActiveDisConnected: Boolean, bleDevice: BleDevice, gatt: BluetoothGatt, status: Int) {
                connectedBleDevice = null
                val why = if (isActiveDisConnected) "actively" else "spontaneously"
                connectionState = EncoderDataSource.EncoderConnectionState.DisconnectedInfo("${machineName} sensor ${bleDevice.name} $why disconnected")
                Log.w(logLabel, "Disconnected via MAC, ${why}, status=${status}")
                android.os.Handler(application.mainLooper).postDelayed(
                    { connectToMAC(sensorMAC) },
                    BleManager.getInstance().reConnectInterval
                )
            }
        })
    }

    /** Sensor reading */

    private fun startNotifying(bleDevice: BleDevice,  gatt: BluetoothGatt) {
        val notifyCallback = object: BleNotifyCallback() {
            override fun onCharacteristicChanged(data: ByteArray) {
                val value = ByteBuffer.wrap(data).short.toLong()
                lastReading = value
            }

            override fun onNotifyFailure(exception: BleException) {
                connectionState = EncoderDataSource.EncoderConnectionState.Error(exception("Cannot read data from ${machineName} sensor ${bleDevice.name}.", exception))
                startNotifying(bleDevice, gatt)
            }

            override fun onNotifySuccess() {
                val name = bleDevice.name ?: gatt.device.name ?: machineIdentity?.sensorTag ?: ""
                connectionState = EncoderDataSource.EncoderConnectionState.ConnectedInfo("${machineName} sensor ${name},\nreceiving data")
            }
        }
        // Start reading the characteristic for raw position after a delay, needed for old tablets.
        // https://stackoverflow.com/questions/47097298/android-ble-bluetoothgatt-writedescriptor-return-sometimes-false
        Log.i(logLabel, "Wait before notifying ${bleDevice.name}.")
        android.os.Handler(application.mainLooper).postDelayed({
            Log.i(logLabel, "Start notifying ${bleDevice.name}.")
            BleManager.getInstance().notify(bleDevice, SERVICE_UUID, SENSOR_CHARACTERISTIC_UUID, notifyCallback)
        }, 10000)
    }

    private val readingObservable =
        BehaviorSubject.create<SensorReading>()

    // Keep the previous time of an incoming reading, in milliseconds.
    private var previousTimeMs = -1L
    // Keep the previous intervals between incoming readings, in milliseconds.
    private var previousIntervalMs = arrayOf(0L, 0L, 0L, 0L)
    // Keep the previous adjusted readings for smoothing.
    private var previousAdjustedReading = arrayOf(0L, 0L, 0L)

    // The last reading (unadjusted) from the sensor.
    private var lastReading: Long = 0
        set(value) {
            // Measure the time interval between readings.
            val currentTimeMs = System.currentTimeMillis()
            val intervalMs = if (previousTimeMs > 0) currentTimeMs - previousTimeMs else 0
            previousTimeMs = currentTimeMs
            // Move the interval into the previous interval queue
            previousIntervalMs.forEachIndexed { index, _ ->
                previousIntervalMs[index] = if (index < previousIntervalMs.lastIndex) previousIntervalMs[index+1] else intervalMs
            }
            // If the last intervals were longer than 80 ms, decrease the interval to 40 ms.
//            if (previousIntervalMs.all {interval -> interval > 80}) {
//                if (connectedBleDevice != null) {
//                    BleManager.getInstance().write(connectedBleDevice, SERVICE_UUID, DATA_RATE_CHARACTERISTIC_UUID,
//                        byteArrayOf(4),
//                        object : BleWriteCallback() {
//
//                            override fun onWriteSuccess(current: Int, total: Int, justWrite: ByteArray?) {
//                                Log.i(logLabel, "Successfully increased the data rate of the sensor")
//                            }
//
//                            override fun onWriteFailure(exception: BleException) {
//                                Log.e(logLabel, "Unable to increase the data rate of the sensor: ${exception.description}")
//                            }
//
//                        }
//                    )
//                }
//            }
            // Change readingOffset when the reading has wrapped around(over- or underflow).
            // This works only when the sensor range is 0 .. 2048.
            val diffReading = value + readingOffset - lastReading
            if (diffReading < -1024) {
                readingOffset  += 2048
                Log.d(logLabel, "Adjusting reading offset to $readingOffset")
            } else if (diffReading > 1024) {
                readingOffset -= 2048
                Log.d(logLabel, "Adjusting reading offset to $readingOffset")
            }
            // Set the adjusted reading.
            val adjustedReading = value + readingOffset
            // Move the adjusted reading into the previous adjusted reading queue.
            previousAdjustedReading.forEachIndexed { index, reading ->
                previousAdjustedReading[index] = if (index < previousAdjustedReading.lastIndex) previousAdjustedReading[index+1] else adjustedReading
            }
            // When possible, smoothen the reading.
            if (adjustedReading > Int.MIN_VALUE && adjustedReading < Int.MAX_VALUE) {
                field = previousAdjustedReading.sum() / previousAdjustedReading.size
                //Log.d(logLabel, "s: ${field.toString()} (${intervalMs}ms)")
            } else {
                field = adjustedReading
                //Log.d(logLabel, "r: ${field.toString()} (${intervalMs}ms)")
            }
            // Let everyone know the new value.
            readingObservable.onNext(field)
        }

    /**
     * Last known sensor position
     * @return last known position
     */
    override fun getLastKnownReading(): SensorReading = lastReading

    /**
     * Encoder reading observable
     * @return [SensorReading] observable
     */
    override fun getObservable(): Flowable<SensorReading> =
        readingObservable.toFlowable(BackpressureStrategy.LATEST)

    /**
     * Reset encoder to zero position is not implemented.
     *
     * @return true if reset operation successful, false - if not
     */
    override fun resetToZero(): Boolean {
        return false
    }

    /**
     * Convert BleException to normal Exception.
     */
    private fun exception(exception: BleException) : Exception {
        return Exception(exception.description)
    }

    private fun exception(info: String, exception: BleException) : Exception {
        return Exception(info + " " + exception.description)
    }

}
