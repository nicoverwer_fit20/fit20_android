package fit20.app.data.wrapper

import android.content.Context
import fit20.app.data.EncoderDataSource
import fit20.app.data.bleEncoder.BleEncoderDataSource
import fit20.app.data.usbEncoder.UsbEncoderDataSource
import fit20.app.exercise.SensorReading
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.inject

/**
 * Class that allow to switch between usb and ble encoder without restarting the app
 *
 * @param context application context
 *
 * @author a.kokuliuk
 * @since v2.1
 */
class WrapperEncoderDataSource(
    private val context: Context
) : EncoderDataSource, KoinComponent {

    // Here we are using field injection to prevent creation of object util we really want to use it
    private val usbEncoderDataSource: EncoderDataSource by inject<UsbEncoderDataSource>()
    private val bleEncoderDataSource: EncoderDataSource by inject<BleEncoderDataSource>()

    private var currentDataSource = BehaviorSubject.create<EncoderDataSource>()
    var currentDataSourceType = BehaviorSubject.create<EncoderDataSourceType>()!!
    private val preferences = context.getSharedPreferences("EncoderType", Context.MODE_PRIVATE)

    private val encoderObservable = BehaviorSubject.create<SensorReading>()
    private val connectionStateObservable =
        BehaviorSubject.create<EncoderDataSource.EncoderConnectionState>()
    private val currentEncoderDisposable = CompositeDisposable()

    init {
        switchDataSource(loadSelectedDataSource())
    }

    /**
     * Select new data source
     * existing @see getObservable observables will return data from newly selected encoder
     *
     * @param newSource new data source
     *
     * @since v2.1
     */
    fun switchDataSource(newSource: EncoderDataSourceType) {
        if ((newSource == EncoderDataSourceType.BLUETOOTH && currentDataSource.value == bleEncoderDataSource) ||
            (newSource == EncoderDataSourceType.USB && currentDataSource.value == usbEncoderDataSource)
        ) {
            //There is no need to select new sensor
            return
        }

        currentDataSource.onNext(
            when (newSource) {
                EncoderDataSourceType.BLUETOOTH -> bleEncoderDataSource
                EncoderDataSourceType.USB -> usbEncoderDataSource
            }
        )
        storeDataSourceType(newSource)
        currentEncoderDisposable.clear()
        currentEncoderDisposable.addAll(
            currentDataSource.value.getObservable()
                .subscribe(encoderObservable::onNext),
            currentDataSource.value.getConnectionStateObservable()
                .subscribe(connectionStateObservable::onNext)
        )
        currentDataSourceType.onNext(newSource)
    }

    /**
     * Get currently selected data source
     *
     * @since v2.1
     */
    fun getCurrentDataSource(): EncoderDataSourceType =
        loadSelectedDataSource()

    private fun storeDataSourceType(newSource: EncoderDataSourceType) {
        preferences.edit().apply {
            putInt("DataSource", newSource.ordinal)
        }.apply()
    }

    private fun loadSelectedDataSource(): EncoderDataSourceType =
        EncoderDataSourceType.values()[preferences.getInt(
            "DataSource",
            EncoderDataSourceType.USB.ordinal
        )]

    override fun getLastKnownReading(): SensorReading =
        currentDataSource.value.getLastKnownReading()

    override fun getObservable(): Flowable<SensorReading> =
        encoderObservable.toFlowable(BackpressureStrategy.LATEST)

    override fun getConnectionStateObservable(): Flowable<EncoderDataSource.EncoderConnectionState> =
        connectionStateObservable.toFlowable(BackpressureStrategy.LATEST)

    override fun resetToZero(): Boolean {
        throw IllegalStateException("Reset operation is not implemented")
    }

    enum class EncoderDataSourceType {
        USB, BLUETOOTH
    }
}