package fit20.app.data.mockEncoder

import fit20.app.data.EncoderDataSource
import fit20.app.exercise.SensorReading
import io.reactivex.rxjava3.core.Flowable
import java.util.concurrent.TimeUnit
import kotlin.math.sin

/**
 * Test purpose only!
 *
 * Encoder source that return hardcoded values
 *
 * @author a.kokuliuk
 * @since v2.1
 */
class MockEncoder(private val scale: Float) : EncoderDataSource {

    override fun getLastKnownReading(): SensorReading {
        return (sin(System.currentTimeMillis().toDouble() / 1000) * scale).toLong()
    }

    override fun getObservable(): Flowable<SensorReading> {
        return Flowable.interval(100, TimeUnit.MILLISECONDS)
            .map {
                (sin(System.currentTimeMillis().toDouble() / 1000) * scale).toLong()
            }
    }

    override fun getConnectionStateObservable(): Flowable<EncoderDataSource.EncoderConnectionState> =
        Flowable.just(EncoderDataSource.EncoderConnectionState.Connected)

    override fun resetToZero(): Boolean {
        TODO("Not implemented")
    }
}
