package fit20.app.data.usbutils

import android.os.Handler
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

suspend inline fun <T> Handler.postSuspendable(crossinline block: () -> T) =
    suspendCancellableCoroutine<T> { continuation ->
        post {
            try{
                val result = block()
                continuation.resume(result)
            }catch (e: Throwable){
                continuation.resumeWithException(e)
            }
        }
    }
