package fit20.app.webserver

/**
 * Web Server
 *
 * Provider REST api for controller application
 *
 * @author a.kokuliuk
 * @since v0.0
 */
interface WebServer {
    /**
     * Start server
     *
     * @since v0.0
     */
    fun start()
}
