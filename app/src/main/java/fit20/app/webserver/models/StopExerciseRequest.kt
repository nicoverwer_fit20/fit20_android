package fit20.app.webserver.models


data class StopExerciseRequest(
    val tempo: Int,
    val rhythm: Int,
    val range: Int,
    val total: Int
)
