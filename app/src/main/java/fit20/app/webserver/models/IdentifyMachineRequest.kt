package fit20.app.webserver.models

data class IdentifyMachineRequest(
    val machineId: String,
    val machineName: String,
    val machineTag: String,
    val sensorTag: String,
    val sensorMAC: String
)
