package fit20.app.webserver.models

/**
 * Exercise configuration request parameters
 *
 * @author a.kokuliuk
 * @since v2.0
 */
data class ConfigureExerciseRequest(
    val member: Member,
    val trackingFactor: Double = 0.0,
    val easingFactor: Double,
    val calibration: Calibration,
    val colorScheme: List<ColorSchemeItem>?,
    val overflow: Double = 0.0,
    val underflow: Double = 0.0
) {
    data class Calibration(
        val start: Long,
        val end: Long
    )
    data class Member(
        val id: String,
        val name: String
    )
    data class ColorSchemeItem(
        val tracking: Float,
        val color: String
    )
}
