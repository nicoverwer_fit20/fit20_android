package fit20.app.webserver

import fit20.app.App
import fit20.app.webserver.components.BaseWebComponent
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.websocket.WebSockets
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * KTOR based @see [WebServer] implementation
 *
 * @property components child components
 *
 * @author a.kokuliuk
 * @since v0.0
 */
class KtorWebServerImpl(
    private val components: Set<BaseWebComponent>
) : WebServer {
    override fun start() {
        GlobalScope.launch {
            launch(App.restDispatcher) {
                embeddedServer(Netty, 1064) {

                    install(ContentNegotiation) {
                        gson { }
                    }

                    install(CORS) {
                        method(HttpMethod.Options)
                        method(HttpMethod.Get)
                        method(HttpMethod.Post)
                        method(HttpMethod.Put)
                        method(HttpMethod.Delete)
                        method(HttpMethod.Patch)
                        header(HttpHeaders.Authorization)
                        allowCredentials = true
                        anyHost()
                    }

                    install(WebSockets) {
                        maxFrameSize = Int.MAX_VALUE.toLong()
                        masking = false
                    }

                    components.forEach {
                        it.setup(this)
                    }
                }.start(wait = true)
            }
        }
    }
}
