package fit20.app.webserver.components

import io.ktor.application.Application

/**
 * Represent a part of WebServer
 *
 * @author a.kokuliuk
 * @since v0.0
 */
interface BaseWebComponent {
    /**
     * Setup component functionality
     *
     * For example: setup new routing pathes, websockets etc
     *
     * @param app KTOR application
     *
     * @since v0.0
     */
    fun setup(app: Application)
}
