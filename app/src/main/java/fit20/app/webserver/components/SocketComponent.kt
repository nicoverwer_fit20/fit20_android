package fit20.app.webserver.components

import android.util.Log
import com.google.gson.Gson
import fit20.app.appState.AppStateInteractor
import fit20.app.data.EncoderDataSource
import fit20.app.exercise.Exercise
import io.ktor.application.Application
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.close
import io.ktor.routing.routing
import io.ktor.websocket.webSocketRaw
import io.reactivex.rxjava3.disposables.CompositeDisposable
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

/**
 * Web socket component
 *
 * @author a.kokuliuk
 * @since v2.0
 */
class SocketComponent(
    private val exercise: Exercise,
    private val stateInteractor: AppStateInteractor,
    private val dataSource: EncoderDataSource
) : BaseWebComponent {
    override fun setup(app: Application) {
        app.apply {
            routing {
                webSocketRaw("/exercise/data", "websocket") {
                    val disposable = CompositeDisposable()
                    disposable.add(stateInteractor.getAppState()
                        .throttleLast(16, TimeUnit.MILLISECONDS)
                        .subscribe {
                            if (it !is AppStateInteractor.AppState.ExerciseState) {
                                disposable.dispose()
                                this@webSocketRaw.launch {
                                    close(CloseReason(CloseReason.Codes.NORMAL, "Close"))
                                }
                                Log.d("SOCKET__", "Closing")
                            }
                        }
                    )

                    disposable.add(
                        dataSource.getConnectionStateObservable()
                            .throttleLast(16, TimeUnit.MILLISECONDS)
                            .subscribe {
                                if (!it.isConnected()) {
                                    this@webSocketRaw.launch {
                                        close(
                                            CloseReason(
                                                CloseReason.Codes.INTERNAL_ERROR,
                                                "Sensor is not connected"
                                            )
                                        )
                                    }
                                }
                            }
                    )

                    Log.d("SOCKET__", this.toString())
                    while (true) {
                        outgoing.send(
                            Frame.Text(
                                Gson().toJson(exercise.getExerciseData()).toString()
                            )
                        )
                        delay(50)
                    }
                }
            }
        }
    }
}
