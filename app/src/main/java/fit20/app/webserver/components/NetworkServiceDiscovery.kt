package fit20.app.webserver.components

import android.app.Application
import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import fit20.app.appState.AppStateInteractor
import fit20.app.exercise.MachineIdentity
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.net.InetAddress

/**
 * This class tries to make the tablet discoverable using DNS-SD / zeroconf / Bonjour.
 * Unfortunately, the Android implementation of DNS-SD does not advertise a meaningful .local address.
 * Instead, it uses Android.local, Android-2.local, and so on.
 * For these .local adrdresses, DNS-SD must be activated.
 * This means that a browser cannot use the .local address for a specific machine.
 * Instead, we implemented the GET /identify service for the browser to discover machines.
 */
class NetworkServiceDiscovery() : KoinComponent, LifecycleObserver {
    private val appState: AppStateInteractor by inject()
    private val application: Application by inject()

    private val SERVICE_TYPE ="_http._tcp." // must have '.' at the end
    private val SERVICE_PORT = 1064

    private var machineServiceName = "" // depends on the machine
    private var registrationListener: NsdManager.RegistrationListener? = null
    private var isRegistered = false;

    private val nsdManager: NsdManager by lazy {application.getSystemService(Context.NSD_SERVICE) as NsdManager}

    init {
        Log.i("NSD_", "network service discovery starting")
        // Start advertising the machine on DNS-SD.
        appState.getMachineIdentity().subscribe { setMachineIdentity(it) }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun exit() {
        Log.i("NSD_", "network service discovery stopping")
        if (isRegistered && registrationListener != null) {
            nsdManager.unregisterService(registrationListener)
        }
    }

    /**
     * When the machine identity is set, change the NSD name.
     */
    private fun setMachineIdentity(machineIdentity: MachineIdentity) {
        if (isRegistered && registrationListener != null) {
            nsdManager.unregisterService(registrationListener)
        }
        if (machineIdentity.machineName != null && machineIdentity.machineName != "") {
            // The registration listener used when the NSD name is registered.
            registrationListener = object : NsdManager.RegistrationListener {
                override fun onServiceRegistered(serviceInfo: NsdServiceInfo) {
                    Log.i("NSD_", "network service registered: ${serviceInfo.serviceName}")
                    isRegistered = true
                    // Now discover our own service. Only when debugging.
                    //discoverSelf()
                }

                override fun onRegistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
                    Log.e(
                        "NSD_",
                        "network service registration failed ($errorCode): ${serviceInfo.serviceName}"
                    )
                    isRegistered = false
                }

                override fun onServiceUnregistered(serviceInfo: NsdServiceInfo) {
                    Log.w("NSD_", "network service unregistered: ${serviceInfo.serviceName}")
                    isRegistered = false
                }

                override fun onUnregistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
                    Log.e(
                        "NSD_",
                        "network service unregistration failed: ${serviceInfo.serviceName}"
                    )
                    isRegistered = true
                }
            }
            machineServiceName = "fit20_" +
                    if (machineIdentity.machineTag == null || "" == machineIdentity.machineTag) {
                        machineIdentity.machineName.replace(Regex("\\s"), "").toLowerCase()
                    } else {
                        machineIdentity.machineTag
                    }
            Log.i("NSD_", "network service registering: ${machineServiceName}")
            val serviceInfo = NsdServiceInfo().apply {
                serviceName = machineServiceName
                serviceType = SERVICE_TYPE
                port = SERVICE_PORT
            }
            nsdManager.registerService(
                serviceInfo,
                NsdManager.PROTOCOL_DNS_SD,
                registrationListener
            )
        }
    }

    /**
     * Only for testing: Discover our own service.
     */
    private fun discoverSelf() {
        val resolveListener = object : NsdManager.ResolveListener {
            override fun onResolveFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
                // Called when the resolve fails. Use the error code to debug.
                Log.e("NSD_", " -- service resolve failed: $errorCode")
            }
            override fun onServiceResolved(serviceInfo: NsdServiceInfo) {
                Log.i("NSD_", " -- service resolve Succeeded. $serviceInfo")
                if (serviceInfo.serviceName == machineServiceName) {
                    Log.i("NSD_", " -- service resolve same IP.")
                }
                Log.i("NSD_", " -- service resolve host: ${serviceInfo.host}  port: ${serviceInfo.port}")
            }
        }

        val discoveryListener = object : NsdManager.DiscoveryListener {
            // Called as soon as service discovery begins.
            override fun onDiscoveryStarted(regType: String) {
                Log.i("NSD_", "service discovery started")
            }
            override fun onServiceFound(service: NsdServiceInfo) {
                Log.i("NSD_", "service discovery success $service")
                when {
                    service.serviceType == SERVICE_TYPE -> {
                        Log.i("NSD_", " -  found service name: ${service.serviceName}")
                        if (service.serviceName.startsWith("fit20") && service.serviceName != machineServiceName) {
                            // Only show details of other tablet. Only works for 1 service.
                            //nsdManager.resolveService(service, resolveListener)
                        }
                    }
                    true -> {
                        Log.i("NSD_", " -  unknown service type: [${service.serviceType}]")
                    }
                }
            }
            override fun onServiceLost(service: NsdServiceInfo) {
                Log.i("NSD_", "service lost: $service")
            }
            override fun onDiscoveryStopped(serviceType: String) {
                Log.i("NSD_", "discovery stopped: $serviceType")
            }
            override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
                Log.i("NSD_", "discovery failed: Error code:$errorCode")
                nsdManager.stopServiceDiscovery(this)
            }
            override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
                Log.i("NSD_", "discovery failed: Error code:$errorCode")
                nsdManager.stopServiceDiscovery(this)
            }
        }

        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener)
    }
}
