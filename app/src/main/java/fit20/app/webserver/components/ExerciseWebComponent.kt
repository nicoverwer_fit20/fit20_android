package fit20.app.webserver.components

import android.graphics.Color
import arrow.core.Either
import com.google.gson.Gson
import fit20.app.appState.AppStateInteractor
import fit20.app.data.EncoderDataSource
import fit20.app.exercise.*
import fit20.app.webserver.models.ConfigureExerciseRequest
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing

/**
 * Exercise configuration and state management API
 *
 * @author a.kokuliuk
 * @since v0.0
 */
class ExerciseWebComponent(
    private val exercise: Exercise,
    private val appStateInteractor: AppStateInteractor,
    private val encoderDataSource: EncoderDataSource
) : BaseWebComponent {

    private fun isSensorConnected() =
        encoderDataSource.getConnectionStateObservable().blockingFirst().isConnected()

    override fun setup(app: Application) {
        app.apply {
            routing {
                post("/configure") {

                    if(!isSensorConnected()){
                        // Without sensor, do not execute the request.
                        call.respond(HttpStatusCode.InternalServerError, "Sensor is not connected")
                        return@post
                    }

                    val rawParams = call.receive<String>()
                    val params = Gson().fromJson<ConfigureExerciseRequest>(
                        rawParams,
                        ConfigureExerciseRequest::class.java
                    )
                    Either.catch {
                        exercise.configure(
                            ExerciseConfiguration(
                                calibration = params.calibration.let {
                                    Calibration(it.start, it.end)
                                },
                                trackingFactor = params.trackingFactor,
                                easingFactor = params.easingFactor,
                                underflow = params.underflow,
                                overflow = params.overflow
                            )
                        )
                    }.fold({
                        call.respond(HttpStatusCode.InternalServerError,  "${it.message}: ${it.cause?.message}")
                    }, {
                        appStateInteractor.setMemberIdentity(MemberIdentity(
                            params.member.id,
                            params.member.name
                        ))
                        params.colorScheme?.map {
                            Pair(it.tracking, Color.parseColor(it.color))
                        }?.toMap()?.also {
                            appStateInteractor.setColorScheme(it)
                        }
                        appStateInteractor.newState(AppStateInteractor.AppState.ExerciseState)
                        call.respond(HttpStatusCode.OK)
                    })
                }

                get("/exercise/start") {

                    if(!isSensorConnected()){
                        // Without sensor, do not execute the request.
                        call.respond(HttpStatusCode.InternalServerError, "Sensor is not connected")
                        return@get
                    }

                    exercise.start()
                    appStateInteractor.newState(AppStateInteractor.AppState.ExerciseState)
                    call.respond(HttpStatusCode.OK)
                }

                get("/exercise/stationary") {
                    exercise.stationary() // even when the sensor has become disconnected

                    if(!isSensorConnected()){
                        call.respond(HttpStatusCode.InternalServerError, "Sensor is not connected")
                        return@get
                    }

                    call.respond(HttpStatusCode.OK)
                }

                get("/exercise/stop") {
                    exercise.stop() // even when the sensor has become disconnected
                    appStateInteractor.newState(AppStateInteractor.AppState.QualityScoreState())

                    if(!isSensorConnected()){
                        call.respond(HttpStatusCode.InternalServerError, "Sensor is not connected")
                        return@get
                    }

                    call.respond(HttpStatusCode.OK)
                }

                post("/exercise/stop") {
                    exercise.stop() // even when the sensor has become disconnected

                    val rawParams = call.receive<String>()
                    val params = Gson().fromJson<QualityScore>(
                        rawParams,
                        QualityScore::class.java
                    )
                    appStateInteractor.newState(AppStateInteractor.AppState.QualityScoreState(params))

                    if(!isSensorConnected()){
                        call.respond(HttpStatusCode.InternalServerError, "Sensor is not connected")
                        return@post
                    }
                    call.respond(HttpStatusCode.OK)
                }

                get("/exercise/finish") {
                    exercise.finish()
                    appStateInteractor.newState(AppStateInteractor.AppState.StandbyState)

                    if(!isSensorConnected()){
                        call.respond(HttpStatusCode.InternalServerError, "Sensor is not connected")
                        return@get
                    }

                    call.respond(HttpStatusCode.OK)
                }

                get("/position"){

                    if(!isSensorConnected()){
                        call.respond(HttpStatusCode.InternalServerError, "Sensor is not connected")
                        return@get
                    }

                    call.respond(
                        mapOf(
                            "position" to exercise.getPosition(),
                            "sensor_reading" to encoderDataSource.getLastKnownReading()
                        )
                    )
                }

            }
        }
    }
}
