package fit20.app.webserver.components

import android.util.Log
import com.google.gson.Gson
import fit20.app.appState.AppStateInteractor
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing

/**
 * Brightness web component
 *
 * @author a.kokuliuk
 * @since v2.0
 */
class BrightnessWebComponent(
    private val appStateInteractor: AppStateInteractor
) : BaseWebComponent {
    override fun setup(app: Application) {
        app.apply {
            routing {
                post("/brightness") {
                    Log.d("REQ_", "Brightness")
                    val rawParams = call.receive<String>()
                    val params = Gson().fromJson<BrightnessRequest>(
                        rawParams,
                        BrightnessRequest::class.java
                    )
                    appStateInteractor.setBrightness(params.level)
                    call.respond(HttpStatusCode.OK)
                }
            }
        }
    }

    internal data class BrightnessRequest(val level: Int)
}
