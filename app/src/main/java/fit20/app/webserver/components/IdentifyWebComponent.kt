package fit20.app.webserver.components

import android.content.Context
import android.net.wifi.WifiManager
import android.text.format.Formatter
import android.util.Log
import com.google.gson.Gson
import fit20.app.appState.AppStateInteractor
import fit20.app.BuildConfig
import fit20.app.exercise.MachineIdentity
import fit20.app.webserver.models.IdentifyMachineRequest
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

/**
 * Identity web component
 *
 * @author b.savrasov
 * @since v2.0
 */
class IdentifyWebComponent(
    private val appStateInteractor: AppStateInteractor
) : BaseWebComponent {
    private var machineIdentity: MachineIdentity? = null
    private val wifiManager: WifiManager by lazy {
        appStateInteractor.getContext().applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
    }

    override fun setup(app: Application) {
        appStateInteractor.getMachineIdentity().subscribe { machineIdentity = it }
        app.apply {
            routing {
                post("/identify") {
                    Log.d("REQ_", "POST /identify")
                    val rawParams = call.receive<String>()
                    val params = Gson().fromJson<IdentifyMachineRequest>(
                        rawParams,
                        IdentifyMachineRequest::class.java
                    )
                    appStateInteractor.setMachineIdentity(
                        MachineIdentity(
                            params.machineId,
                            params.machineName,
                            params.machineTag ?: machineIdentity?.machineTag,
                            params.sensorTag ?: machineIdentity?.sensorTag,
                            params.sensorMAC ?: machineIdentity?.sensorMAC
                        )
                    )
                    call.respond(HttpStatusCode.OK)
                }

                get("/identify") {
                    Log.d("REQ_", "GET /identify")
                    call.respond(
                        mapOf(
                            "machineId" to machineIdentity?.machineId,
                            "machineName" to machineIdentity?.machineName,
                            "machineTag" to machineIdentity?.machineTag,
                            "sensorTag" to machineIdentity?.sensorTag,
                            "sensorMAC" to machineIdentity?.sensorMAC,
                            "IPAddress" to  Formatter.formatIpAddress(wifiManager.connectionInfo.ipAddress),
                            "appVersion" to BuildConfig.VERSION_NAME
                        )
                    )
                }
            }
        }
    }

    internal data class BrightnessRequest(val level: Int)
}
