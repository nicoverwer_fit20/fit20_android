package fit20.app.exercise

import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import kotlin.math.abs

class ExerciseImplTest {

    private val initialCalibrationStart : SensorPosition = 3142
    private val initialCalibrationEnd : SensorPosition = -2718
    private val initialRestingReading : SensorReading = 1618

    private val initialConfiguration = ExerciseConfiguration(
        Calibration(initialCalibrationStart, initialCalibrationEnd), 10.0, 1.5, 0.1, 0.1
    )

    private fun calibrated(calibration: Calibration, position: SensorPosition): CalibratedPosition {
        return (position - calibration.start).toDouble() / (calibration.end - calibration.start)
    }

    @Before
    fun setUp() {
        ExerciseImpl.onSensorReadingChanged(initialRestingReading) // This sets the resting position.
        ExerciseImpl.configure(initialConfiguration)
    }

    @After
    fun tearDown() {
    }

    @Test
    fun getPosition() {
        // After setup, the sensor has not moved away from the initial resting position, and position is zero.
        assertEquals(0, ExerciseImpl.getPosition())
        ExerciseImpl.onSensorReadingChanged(initialRestingReading + 314)
        assertEquals(314, ExerciseImpl.getPosition())
        ExerciseImpl.onSensorReadingChanged(initialRestingReading - 272)
        assertEquals(-272, ExerciseImpl.getPosition())
    }

    @Test
    fun configure() {
        val myCalibration = Calibration(123, 223)
        val myConfiguration = ExerciseConfiguration(
            myCalibration, 10.0, 1.5, 0.1, 0.1
        )
        ExerciseImpl.onSensorReadingChanged(initialRestingReading + 42)
        ExerciseImpl.configure(myConfiguration)
        // Test increasing the reading by 42.
        assertEquals(42, ExerciseImpl.getPosition())
        ExerciseImpl.onSensorReadingChanged(initialRestingReading - 5)
        assertEquals(-5, ExerciseImpl.getPosition())
        // Test the calibration and count.
        val exerciseData = ExerciseImpl.getExerciseData()
        assertEquals(calibrated(myCalibration, -5), exerciseData.actualPosition, 0.01)
        assertEquals(null, exerciseData.count)
        ExerciseImpl.onSensorReadingChanged(initialRestingReading + 162)
        assertEquals(calibrated(myCalibration, 162), ExerciseImpl.getExerciseData().actualPosition, 0.01)
    }

    @Test
    fun start() {
        ExerciseImpl.start()
        val exData = ExerciseImpl.getExerciseData()
        // During the count-down, count is negative, reference position is zero, half-rep is zero.
        assertEquals(-3 ,exData.count)
        assertEquals(0.0, exData.referencePosition, 0.1)
        assertEquals(0, exData.halfRepetition)
    }

    @Test
    fun stationary() {
        ExerciseImpl.start()
        val exData1 = ExerciseImpl.getExerciseData()
        // Wait until 3 seconds into actual movement.
        val startTimeMillis = exData1.time
        val waitingMillis = 3000 - startTimeMillis
        Thread.sleep(waitingMillis)
        ExerciseImpl.stationary()
        val exData2 = ExerciseImpl.getExerciseData()
        // During stationary, count starts at 10, half-rep is -1.
        assertEquals(10, exData2.count)
        assertEquals(-1, exData2.halfRepetition)
        // Wait one second, then reference position must not have changed, count goes down.
        Thread.sleep(1000)
        val exData3 = ExerciseImpl.getExerciseData()
        assertEquals(exData2.referencePosition, exData3.referencePosition, 0.01)
        assertEquals(9, exData3.count)
        // Wait 10 seconds, then reference position must not have changed, count is zero.
        Thread.sleep(10000)
        val exData4 = ExerciseImpl.getExerciseData()
        assertEquals(exData2.referencePosition, exData4.referencePosition, 0.01)
        assertEquals(0, exData4.count)
    }

    @Test
    fun stop() {
        ExerciseImpl.start()
        ExerciseImpl.stop()
        val exData1 = ExerciseImpl.getExerciseData()
        // After stop, half-rep is 0.
        assertEquals(0, exData1.halfRepetition)
        // Now test going through stationary.
        ExerciseImpl.start()
        ExerciseImpl.stationary()
        ExerciseImpl.stop()
        val exData2 = ExerciseImpl.getExerciseData()
        // After stop, half-rep is 0 and count is null.
        assertEquals(0, exData2.halfRepetition)
        assertEquals(null, exData2.count)
    }

    @Test
    fun finish() {
        // There is nothing interesting to test here.
    }

    @Test
    fun getExerciseData() {
        // Test the time, referencePosition, trackingFactor.
        // Using real time for testing is unreliable, so use wide margins.
        ExerciseImpl.start()
        val exData1 = ExerciseImpl.getExerciseData()
        // Wait until 5 seconds into actual movement.
        val startTimeMillis = exData1.time
        val waitingMillis = 5000 - startTimeMillis
        Thread.sleep(waitingMillis)
        // Then read the exercise data.
        ExerciseImpl.onSensorReadingChanged(initialCalibrationStart + (initialCalibrationEnd - initialCalibrationStart) / 11)
        val exData2 = ExerciseImpl.getExerciseData()
        assertEquals(5.0, exData2.count!!.toDouble(), 1.0)
        assertEquals(waitingMillis.toDouble() / 1000, (exData2.time - startTimeMillis).toDouble() / 1000, 0.5)
        assertEquals(0.5, exData2.referencePosition, 0.1)
        assertEquals(abs(exData2.actualPosition - exData2.referencePosition) * initialConfiguration.trackingFactor, exData2.tracking, 0.01)
    }
}