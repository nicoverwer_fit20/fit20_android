#Pre PUSH checks
- run linters
- execute unit tests
- execute UI tests

# Code style
## Class annotation
Every class must be commented. Sample:
```
/**
* Short description
*
* Description
*
* @author author name
* @version version in which class was appeared
*/
```

## Method annotation
Public methods should be commented. Sample:
```
/**
* Short description
*
* Optional: description
*
* @params param1 param1 description
* @params param2 param2 description
*
* @since version in which method is appeared
*/
```

### Method with side effects
Dirty methods must be marked as suspend (if suspend have no impact on performance)
Avoid of throwing exception, if business logic requires to throw exception use *Either* class

# Commit style
Use Angular commit format
```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Example:
```
fix(release): need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.

Closes #PROJ-1
```
